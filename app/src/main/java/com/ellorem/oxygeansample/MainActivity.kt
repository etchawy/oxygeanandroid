package com.ellorem.oxygeansample

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.ellorem.oxygean.video.ExampleActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<Button>(R.id.btn).setOnClickListener {
            val myIntent = Intent(this@MainActivity, ExampleActivity::class.java)
            this@MainActivity.startActivity(myIntent)
        }
    }

}