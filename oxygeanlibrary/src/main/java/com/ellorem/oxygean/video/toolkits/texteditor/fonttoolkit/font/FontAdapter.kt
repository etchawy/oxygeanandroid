package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.ellorem.oxygean.video.R

class FontAdapter(private val listener: OnFontClickListener, context: Context) :
        RecyclerView.Adapter<FontIconViewHolder>() {

    private val listOfFonts = listOf(
            Font(R.drawable.ic_text_sample_1_a, R.drawable.ic_text_sample_1_p,
                    ResourcesCompat.getFont(context, R.font.strong_medium)!!),
            Font(R.drawable.ic_text_sample_2_a, R.drawable.ic_text_sample_2_p,
                    ResourcesCompat.getFont(context, R.font.fun_regular)!!),
            Font(R.drawable.ic_text_sample_3_a, R.drawable.ic_text_sample_3_p,
                    ResourcesCompat.getFont(context, R.font.cajzh_medium)!!),
            Font(R.drawable.ic_text_sample_4_a, R.drawable.ic_text_sample_4_p,
                    ResourcesCompat.getFont(context, R.font.serif_regular)!!),
            Font(R.drawable.ic_text_sample_5_a, R.drawable.ic_text_sample_5_p,
                    ResourcesCompat.getFont(context, R.font.novel_medium)!!))

    private var switchedFont = listOfFonts[0]

    fun getFirst(): Font = listOfFonts[0]

    fun switchFont(font: Font) {
        switchedFont = font
        listOfFonts.forEach { it.isSwitched = false }
        listOfFonts.first { it.offPictureRes == font.offPictureRes }.isSwitched = true
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FontIconViewHolder =
            FontIconViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_font_in_editor, parent, false), listener)

    override fun onBindViewHolder(holder: FontIconViewHolder, position: Int) =
            holder.bind(listOfFonts[position])

    override fun getItemCount(): Int = listOfFonts.size

}