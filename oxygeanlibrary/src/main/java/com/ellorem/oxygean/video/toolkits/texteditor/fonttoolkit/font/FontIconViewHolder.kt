package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.ellorem.oxygean.video.R

class FontIconViewHolder(private val itemRow: View, private val listener: OnFontClickListener) :
        RecyclerView.ViewHolder(itemRow) {

    private val fontIcon: ImageView = itemRow.findViewById(R.id.font_sample_icon)

    fun bind(font: Font) {
        fontIcon.setImageResource(if (font.isSwitched) font.onPictureRes else font.offPictureRes)

        itemRow.setOnClickListener {
            listener.onClick(font)
        }
    }
}