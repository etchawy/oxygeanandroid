package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font

interface OnFontClickListener {
    fun onClick(font: Font)
}