package com.ellorem.oxygean.video.base;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.ellorem.oxygean.video.ThumbnailAdapter;

import kotlin.jvm.JvmOverloads;

public class ItemTouchCallback extends ItemTouchHelper.Callback {
    public boolean isSwipingDown = false;
    public boolean isClick = false;

    public float prevDx = 0F;
    public float prevDy = 0F;
    ThumbnailAdapter adapter;

    public ItemTouchCallback(ThumbnailAdapter adapter){
        this.adapter = adapter;
    }

    @Override
    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        int dragFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN;
        int swipeFlags = ItemTouchHelper.DOWN;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        adapter.onRowMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true ;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

    }


    @JvmOverloads
    public void clearView(@Nullable RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder){
        if(recyclerView==null)
            return;
        if (prevDy > 100) {
            isSwipingDown = true;
        }
        else if (prevDy < 20 && prevDx < 20 ) {
            isClick = true;
        }
        prevDx = 0F;
        prevDy = 0F;
        if (isSwipingDown) {
            adapter.swipeToDismiss(viewHolder.getAdapterPosition());
            isSwipingDown = false;
        }
        if (isClick) {
            adapter.click(viewHolder);
            isClick = false;
        }
        super.clearView(recyclerView, viewHolder);
        if (viewHolder instanceof ThumbnailAdapter.ThumbnailViewHolder) {
            adapter.onRowClear((ThumbnailAdapter.ThumbnailViewHolder) viewHolder);
        }
    }

}
