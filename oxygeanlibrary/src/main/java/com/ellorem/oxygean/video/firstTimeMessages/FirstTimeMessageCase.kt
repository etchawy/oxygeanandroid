package com.ellorem.oxygean.video.firstTimeMessages

import com.ellorem.oxygean.video.R

enum class FirstTimeMessageCase(val textId: Int, val prefId: String?, val delay: Int = 0) {

    MINIMUM_RECORD(R.string.minimum_record_time, null),
    CAMERA_CASE(0, FIRST_TIME_CAMERA),
    ADD_NEW_VIDEO_CASE_STOPPED_BY_USER(R.string.ftm_add_new_video_step, ADD_NEW_VIDEO_STEP_USER),
    ADD_NEW_VIDEO_CASE_AUTO_STOP(R.string.ftm_add_new_video_step, ADD_NEW_VIDEO_STEP_AUTO),
    INACTIVITY_SHORT(R.string.ftm_inactive_short, INACTIVITY_SHORT_MESSAGE, R.integer.inactivity_short_delay),
    INACTIVITY_MEDIUM(R.string.ftm_inactive_medium, INACTIVITY_MEDIUM_MESSAGE, R.integer.inactivity_medium_delay),
    INACTIVITY_LONG(R.string.ftm_inactive_long, INACTIVITY_LONG_MESSAGE, R.integer.inactivity_long_delay)

}

private const val FIRST_TIME_CAMERA = "first_time_camera"
private const val ADD_NEW_VIDEO_STEP_USER = "add_new_video_step_user"
private const val ADD_NEW_VIDEO_STEP_AUTO = "add_new_video_step_auto"
private const val INACTIVITY_SHORT_MESSAGE = "inactivity_short_message"
private const val INACTIVITY_MEDIUM_MESSAGE = "inactivity_medium_message"
private const val INACTIVITY_LONG_MESSAGE = "inactivity_long_message"