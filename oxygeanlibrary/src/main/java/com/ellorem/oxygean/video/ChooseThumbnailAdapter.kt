package com.ellorem.oxygean.video

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.ellorem.oxygean.video.R
import com.ellorem.oxygean.video.util.MediaUtil
import java.io.FileOutputStream
import java.io.IOException


class ChooseThumbnailAdapter(applicationContext: Context, logos: List<MediaUtil.VideoData>) : BaseAdapter() {
    private var context: Context
    private val videos: List<MediaUtil.VideoData>
    val forUpload: MutableList<MediaUtil.VideoData> = mutableListOf()
    private val inflater: LayoutInflater
    val listeners = mutableListOf<OnChooseThumbnailListener>()

    override fun getCount(): Int {
        return videos.size
    }

    override fun getItem(i: Int): Any? {
        return null
    }

    override fun getItemId(i: Int): Long {
        return 0
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup?): View {
        var layoutVew = inflater.inflate(R.layout.choose_thumbnail_item, null)

        val path = videos.get(i).videoURL;

        val icon = layoutVew.findViewById<ImageView>(R.id.icon)
        Glide.with(context).load(path).into(icon)

        val duration = layoutVew.findViewById<TextView>(R.id.duration)
        duration.setText(String.format(context.getString(R.string.duration_format), Math.round(videos.get(i).length)))

        if (forUpload.contains(videos.get(i))) {
            layoutVew.findViewById<ImageView>(R.id.checked).setImageResource(R.mipmap.checked)
        }

        layoutVew.findViewById<View>(R.id.item_filter_preview).setOnClickListener {
            val wasChecked = forUpload.contains(videos.get(i))
            if (!wasChecked && forUpload.size == 10) {
                //limit for number of files to be checked
                return@setOnClickListener
            }
            it.findViewById<ImageView>(R.id.checked).setImageResource(if (!wasChecked!!) R.mipmap.checked else R.mipmap.unchecked)
            if (!wasChecked) {
                forUpload.add(videos.get(i))
                val thumbFile = MediaUtil.createFile(context, "jpeg")
                Glide.with(context)
                        .asBitmap()
                        .load(videos.get(i).videoURL)
                        .into(object : CustomTarget<Bitmap>(){
                            override fun onResourceReady(resource: Bitmap, transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?) {
                                try {
                                    FileOutputStream(thumbFile.absolutePath).use { out ->
                                        resource.compress(Bitmap.CompressFormat.JPEG, 100, out)
                                        videos.get(i).thumbURL = thumbFile.absolutePath
                                    }
                                } catch (e: IOException) {
                                    e.printStackTrace()
                                }
                            }

                            override fun onLoadCleared(placeholder: Drawable?) {

                            }
                        })
                if (forUpload.size == 1){
                    for (listener in listeners) {
                        listener.onHasChecks()
                    }
                }

            } else {
                forUpload.remove(videos.get(i))
                if (forUpload.size == 0){
                    for (listener in listeners) {
                        listener.onNoChecks()
                    }
                }

            }
        }

        return layoutVew
    }

    init {
        context = applicationContext
        videos = logos
        inflater = LayoutInflater.from(applicationContext)
    }

    fun addListener(onChooseThumbnailListener: OnChooseThumbnailListener) {
        listeners.add(onChooseThumbnailListener)
    }

    fun removeListener(onChooseThumbnailListener: OnChooseThumbnailListener) {
        listeners.remove(onChooseThumbnailListener)
    }

    interface OnChooseThumbnailListener {
        fun onHasChecks()
        fun onNoChecks()
    }
}