package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.color

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.ellorem.oxygean.video.R

class ColorIconViewHolder(private val itemRow: View, private val listener: OnColorClickListener) :
        RecyclerView.ViewHolder(itemRow) {

    private val colorIcon: ImageView = itemRow.findViewById(R.id.color_icon)
    private val colorChecked: ImageView = itemRow.findViewById(R.id.color_icon_checked)

    fun bind(color: Color) {
        colorIcon.setImageResource(color.colorIconId)
        colorChecked.visibility = if (color.isSwitched) View.VISIBLE else View.GONE

        itemRow.setOnClickListener {
            listener.onClick(color)
        }
    }
}