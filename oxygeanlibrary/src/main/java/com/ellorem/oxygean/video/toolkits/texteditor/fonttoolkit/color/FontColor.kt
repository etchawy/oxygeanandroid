package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.color

import com.ellorem.oxygean.video.R

sealed class FontColor(val colorIconId: Int, val colorId: Int, val secondColorId: Int = 0) {

    var isSelected: Boolean = false

    object White : FontColor(R.drawable.ic_color_container_white, R.color.text_toolkit_color_black)
    object Black : FontColor(R.drawable.ic_color_container_black, R.color.text_toolkit_color_white)
    object BrandRed: FontColor(R.drawable.ic_color_container_brand_red, R.color.text_toolkit_color_white)
    object CommStyle : FontColor(R.drawable.ic_color_container_comms_style, R.color.text_toolkit_color_white)
    object CompPower : FontColor(R.drawable.ic_color_container_comp_power, R.color.text_toolkit_color_white)
    object EmoIntel : FontColor(R.drawable.ic_color_container_emo_intel, R.color.text_toolkit_color_white)
    object FixingFlair : FontColor(R.drawable.ic_color_container_fixing_flair, R.color.text_toolkit_color_white)
    object HackKnack : FontColor(R.drawable.ic_color_container_hack_knack, R.color.text_toolkit_color_white)
    object Knowledge : FontColor(R.drawable.ic_color_container_knowledge, R.color.text_toolkit_color_white)
    object MyClout : FontColor(R.drawable.ic_color_container_my_clout, R.color.text_toolkit_color_white)
    object MyMoxy : FontColor(R.drawable.ic_color_container_my_moxy, R.color.text_toolkit_color_white)
    object Purple : FontColor(R.drawable.ic_color_container_purple, R.color.text_toolkit_color_white)
    object ActBarom : FontColor(R.drawable.ic_color_container_act_barom, R.color.text_toolkit_color_white)

    fun provideColorsList(): List<FontColor> = listOf()
}