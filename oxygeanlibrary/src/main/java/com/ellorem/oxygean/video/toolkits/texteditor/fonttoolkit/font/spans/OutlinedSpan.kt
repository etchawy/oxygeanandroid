package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font.spans

import android.graphics.Canvas
import android.graphics.Paint
import android.text.style.ReplacementSpan
import android.text.style.UpdateAppearance
import androidx.annotation.ColorInt
import androidx.annotation.Dimension

class OutlinedSpan(
        @ColorInt private val textColor: Int,
        @ColorInt private val strokeColor: Int,
        @Dimension private val strokeWidth: Float
): ReplacementSpan(), UpdateAppearance {

    override fun getSize(
            paint: Paint,
            text: CharSequence,
            start: Int,
            end: Int,
            fm: Paint.FontMetricsInt?
    ): Int {
        return paint.measureText(text.toString().substring(start until end)).toInt()
    }

    override fun draw(
            canvas: Canvas,
            text: CharSequence,
            start: Int,
            end: Int,
            x: Float,
            top: Int,
            y: Int,
            bottom: Int,
            paint: Paint
    ) {
        paint.apply {
            color = strokeColor
            style = Paint.Style.STROKE
            this.strokeWidth = this@OutlinedSpan.strokeWidth
        }
        canvas.drawText(text, start, end, x, y.toFloat(), paint)

        paint.apply {
            color = textColor
            style = Paint.Style.FILL
        }
        canvas.drawText(text, start, end, x, y.toFloat(), paint)
    }
}