package com.ellorem.oxygean.video.toolkits.videofilter

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.ellorem.oxygean.video.util.dpToPx


class FilterPreviewItemDecorator(val context: Context, private val filterListSize: Int) : RecyclerView.ItemDecoration() {

    companion object {
        private const val ICON_SIZE = 50
    }

    override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)
        val displayMetrics = context.resources.displayMetrics
        val offset = (displayMetrics.widthPixels / 2) - (ICON_SIZE / 2).dpToPx()
        if (position == 0) {
            outRect.left = offset
        }
        if (position == filterListSize - 1) {
            outRect.right = offset
        }
    }
}