package com.ellorem.oxygean.video.util

object Constants {

    const val EXTENSION_MP4 = "mp4"
    const val EXTENSION_PNG = "png"
    const val PREFIX_FILTERED = "FILTERED_"
    const val WITH_TEXT_STICKER = "T_STICKER_"

}