package com.ellorem.oxygean.video.videodata

import com.zomato.photofilters.geometry.Point
import com.zomato.photofilters.imageprocessors.Filter
import com.zomato.photofilters.imageprocessors.subfilters.BrightnessSubFilter
import com.zomato.photofilters.imageprocessors.subfilters.ContrastSubFilter
import com.zomato.photofilters.imageprocessors.subfilters.SaturationSubFilter
import com.zomato.photofilters.imageprocessors.subfilters.ToneCurveSubFilter

object CustomImageFilters {

    val imageMonoFilter = Filter()
            .apply {
                addSubFilter(SaturationSubFilter((-0.99).toFloat()))
            }

    val imageCoolFilter = Filter()
            .apply {
                addSubFilter(ContrastSubFilter(1.1f))
                addSubFilter(SaturationSubFilter(0.9f))
                addSubFilter(ToneCurveSubFilter(getIncreasedGamma(), getColdRed(), null, getColdBlue()))
            }

    val imageWarmFilter = Filter()
            .apply {
                addSubFilter(ContrastSubFilter(1.1f))
                addSubFilter(SaturationSubFilter(1.1f))
                addSubFilter(ToneCurveSubFilter(getIncreasedGamma(), getWarmRed(), null, null))
            }

    val imageClarityFilter = Filter()
            .apply {
                addSubFilter(BrightnessSubFilter(30))
                addSubFilter(SaturationSubFilter(0.9f))
                addSubFilter(ToneCurveSubFilter(null, null, null, getColdBlue()))
            }

    private fun getIncreasedGamma(): Array<Point?> {
        val rgbKnots: Array<Point?> = arrayOfNulls(3)
        rgbKnots[0] = Point(0f, 0f)
        rgbKnots[1] = Point(113f, 129f)
        rgbKnots[2] = Point(255f, 255f)
        return rgbKnots
    }

    private fun getColdRed(): Array<Point?> {
        val redKnots: Array<Point?> = arrayOfNulls(3)
        redKnots[0] = Point(0f, 0f)
        redKnots[1] = Point(121f, 110f)
        redKnots[2] = Point(255f, 255f)
        return redKnots
    }

    private fun getColdBlue(): Array<Point?> {
        val blueKnots: Array<Point?> = arrayOfNulls(3)
        blueKnots[0] = Point(0f, 0f)
        blueKnots[1] = Point(130f, 173f)
        blueKnots[2] = Point(255f, 255f)
        return blueKnots
    }

    private fun getWarmRed(): Array<Point?> {
        val redKnots: Array<Point?> = arrayOfNulls(3)
        redKnots[0] = Point(0f, 0f)
        redKnots[1] = Point(149f, 117f)
        redKnots[2] = Point(255f, 255f)
        return redKnots
    }
}