package com.ellorem.oxygean.video.videodata

import android.content.Context
import android.util.Log
import com.arthenica.mobileffmpeg.Config
import com.arthenica.mobileffmpeg.FFmpeg
import com.arthenica.mobileffmpeg.FFmpeg.RETURN_CODE_CANCEL
import com.arthenica.mobileffmpeg.FFmpeg.RETURN_CODE_SUCCESS
import com.ellorem.oxygean.video.toolkits.texteditor.TextSticker
import com.ellorem.oxygean.video.toolkits.videofilter.VideoFilter
import com.ellorem.oxygean.video.toolkits.videofilter.ffmpegFilterConvertForComplex
import java.io.File
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

const val FFMPEG_TAG = "FFmpegCommand"

class FilterAndImageOverlayUseCase {

    suspend fun addFilterAndImageToVideo(
            inVideo: File,
            outVideo: File,
            sticker: TextSticker,
            filter: VideoFilter,
            context: Context
    ): Unit = suspendCoroutine { it.resume(addFilterAndImage(context, inVideo, outVideo, sticker, filter)) }

    private fun addFilterAndImage(context: Context,
                                  inVideo: File,
                                  outVideo: File,
                                  textSticker: TextSticker,
                                  filter: VideoFilter) {

        val size = textSticker.sizePX
        val startXp = textSticker.xP
        val startYp = textSticker.yP
        val imageFile = textSticker.imageFile
        val filterCommand = ffmpegFilterConvertForComplex(context, filter)
        val s = if (size % 2 == 0) size else size + 1
        val text = ";[s][1:v]overlay=${startXp}*W:${startYp}*H,scale=720:-2"
        val commands = arrayOf("-y",
                "-i", inVideo.absolutePath,
                "-i", imageFile.absolutePath,
                "-filter_complex",
                "[0:v]pad=width=ceil(iw/2)*2:height=ceil(ih/2)*2, $filterCommand[s]$text",
                "-movflags", "faststart",
                "-vcodec", "libx264",
                "-preset:v", "ultrafast",
                "-profile:v", "high",
                "-pix_fmt", "yuv420p",
                "-crf:v", "30",
                outVideo.absolutePath)

        Log.d(FFMPEG_TAG, "start execute filter and text")
        when (val rc = FFmpeg.execute(commands)) {
            RETURN_CODE_SUCCESS -> Log.i(Config.TAG, "Command execution completed successfully.")
            RETURN_CODE_CANCEL -> Log.i(Config.TAG, "Command execution cancelled by user.")
            else -> {
                Log.i(Config.TAG, "Command execution failed with $rc and the output below.")
            }
        }
        Log.d(FFMPEG_TAG, "finish execute filter and text")
        inVideo.delete()
        imageFile.delete()
    }
}