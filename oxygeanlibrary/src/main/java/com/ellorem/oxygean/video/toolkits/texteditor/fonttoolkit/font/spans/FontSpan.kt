package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font.spans

import android.graphics.Typeface
import android.text.TextPaint
import android.text.style.MetricAffectingSpan

class FontSpan(
        private val font: Typeface,
        private val size: Float
) : MetricAffectingSpan() {

    override fun updateDrawState(textPaint: TextPaint) = updateTypeface(textPaint)

    override fun updateMeasureState(textPaint: TextPaint) = updateTypeface(textPaint)

    private fun updateTypeface(textPaint: TextPaint) {
        textPaint.apply {
            typeface = Typeface.create(font, font.style)
            textSize = size
        }
    }
}