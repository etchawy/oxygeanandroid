package com.ellorem.oxygean.video.videodata

import com.ellorem.oxygean.video.toolkits.videofilter.VideoFilter

data class VideoFilterAccepted(
        var tempFilter: VideoFilter = VideoFilter.ORIGINAL,
        var appliedFilter: VideoFilter = VideoFilter.ORIGINAL,
        var isTempApplied: Boolean = false,
        var isApplied: Boolean = false
) {
    fun setData(videoFilterAccepted: VideoFilterAccepted) {
        this.tempFilter = videoFilterAccepted.tempFilter
        this.appliedFilter = videoFilterAccepted.appliedFilter
        this.isTempApplied = videoFilterAccepted.isTempApplied
        this.isApplied = videoFilterAccepted.isApplied
    }
}