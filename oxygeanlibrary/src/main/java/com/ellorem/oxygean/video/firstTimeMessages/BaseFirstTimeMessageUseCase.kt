package com.ellorem.oxygean.video.firstTimeMessages

import android.content.Context
import android.util.Log

abstract class BaseFirstTimeMessageUseCase {

    protected fun checkIsItFirstTime(context: Context, case: FirstTimeMessageCase): Boolean {
        if (case.prefId == null) return true
        val pref = context.getSharedPreferences(FIRST_TIME_PREFS, Context.MODE_PRIVATE)
        if (pref.getBoolean(case.prefId, true)) {
            pref.edit().putBoolean(case.prefId, false).apply()
            return true
        }
        return false
    }

    companion object {
        private const val FIRST_TIME_PREFS = "first_time_prefs"
    }
}