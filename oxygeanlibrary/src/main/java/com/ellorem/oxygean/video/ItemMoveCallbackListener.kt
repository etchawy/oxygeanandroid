package com.ellorem.oxygean.video

import android.graphics.Canvas
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.ellorem.oxygean.video.base.ItemTouchCallback

class ItemMoveCallbackListener(private val adapter: ThumbnailAdapter) : ItemTouchCallback(adapter) {

    override fun isItemViewSwipeEnabled(): Boolean {
        return true
    }

    override fun isLongPressDragEnabled(): Boolean {
        return false
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {

        if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
            if (viewHolder is ThumbnailAdapter.ThumbnailViewHolder) {
                adapter.onRowSelected(viewHolder)
            }
        }
        super.onSelectedChanged(viewHolder, actionState)
    }

    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        //check for the biggest user swipe
        if (prevDx < dX) {
            prevDx = dX
        }
        if (prevDy < dY) {
            prevDy = dY
        }

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

    interface Listener {
        fun onRowMoved(fromPosition: Int, toPosition: Int)
        fun onRowSelected(itemViewHolder: ThumbnailAdapter.ThumbnailViewHolder)
        fun onRowClear(itemViewHolder: ThumbnailAdapter.ThumbnailViewHolder)
    }
}