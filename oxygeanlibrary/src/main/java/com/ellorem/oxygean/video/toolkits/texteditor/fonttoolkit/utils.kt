package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit

import android.widget.EditText
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font.FontSize

fun EditText.increase(fontSize: FontSize): FontSize =
    when(fontSize){
        FontSize.SMALL -> FontSize.MEDIUM
        FontSize.MEDIUM -> FontSize.LARGE
        FontSize.LARGE -> FontSize.LARGE
    }


fun EditText.reduce(fontSize: FontSize): FontSize =
    when(fontSize){
        FontSize.LARGE -> FontSize.MEDIUM
        FontSize.MEDIUM -> FontSize.SMALL
        FontSize.SMALL -> FontSize.SMALL
    }