package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font

import com.ellorem.oxygean.video.R

enum class FontSize(val id: Int, val value: Float, val borderWidth: Float, val imageId: Int) {
    SMALL(0, 20f, 4f, R.drawable.ic_font_size),
    MEDIUM(1, 40f, 7f, R.drawable.ic_font_size),
    LARGE(2, 60f, 10f, R.drawable.ic_font_size);

    companion object {
        fun getSizeById(id: Int) = values()[id]
    }
}