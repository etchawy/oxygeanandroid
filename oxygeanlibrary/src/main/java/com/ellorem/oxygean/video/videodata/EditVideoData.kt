package com.ellorem.oxygean.video.videodata

import com.ellorem.oxygean.video.toolkits.texteditor.TextSticker
import com.ellorem.oxygean.video.util.MediaUtil

data class EditVideoData(var video: MediaUtil.VideoData,
                         var isSelected: Boolean = false,
                         var isTemporaryForSplit: Boolean = false,
                         var isOriginalForSplit: Boolean = false,
                         val videoFilterAccepted: VideoFilterAccepted = VideoFilterAccepted()
) {
    var textSticker: TextSticker? = null
    var sticker: TextSticker? = null
    var poll: TextSticker? = null
}

fun makeCopyOf(evd: EditVideoData): EditVideoData =
        EditVideoData(
                evd.video,
                isSelected = false,
                isOriginalForSplit = false,
                isTemporaryForSplit = false
        ).apply {
            this.textSticker = evd.textSticker
            this.videoFilterAccepted.setData(evd.videoFilterAccepted)
            this.sticker = evd.sticker
            this.poll = evd.poll
        }