package com.ellorem.oxygean.video.videodata

import android.content.Context
import android.util.Log
import com.arthenica.mobileffmpeg.Config
import com.arthenica.mobileffmpeg.FFmpeg
import com.arthenica.mobileffmpeg.FFmpeg.RETURN_CODE_CANCEL
import com.arthenica.mobileffmpeg.FFmpeg.RETURN_CODE_SUCCESS
import com.ellorem.oxygean.video.toolkits.videofilter.VideoFilter
import com.ellorem.oxygean.video.toolkits.videofilter.ffmpegFilterConvert
import java.io.File
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class VideoFilterUseCase {

    suspend fun filterVideo(context: Context, editedFile: File, saveTo: File, filter: VideoFilter): Unit =
            suspendCoroutine { it.resume(applyFilterForFile(context, editedFile, saveTo, filter)) }

    private fun applyFilterForFile(context: Context, editedFile: File, saveTo: File, filter: VideoFilter) {
//        val command = "-y -i " + editedFile.absolutePath + " -vf hue=s=0 -vcodec libx264 -preset ultrafast -crf 24  -b:a 96k " + saveTo.absolutePath
//        val command = "-y -i " + editedFile.absolutePath + " -vf hue=s=0 -b 2097152 " + saveTo.absolutePath
//        val command = "-y -i " + editedFile.absolutePath + " -vf curves=psfile=/storage/emulated/0/Android/data/com.ellorem.oxygean.video/files/darken.acv -b 2097152 " + saveTo.absolutePath
//        val command = "-y -i " + editedFile.absolutePath + " " + ffmpegFilterConvert(context, filter) + " " + saveTo.absolutePath

//        val command = "-y -i ${editedFile.absolutePath} ${ffmpegFilterConvert(context, filter)} scale=iw/2:ih/2 -vcodec libx264 -crf 18 ${saveTo.absolutePath}"
        val command = "-y -i ${editedFile.absolutePath} ${ffmpegFilterConvert(context, filter)} -vcodec libx264 -preset:v ultrafast -profile:v high -pix_fmt yuv420p -crf 30 ${saveTo.absolutePath}"

        Log.d("VideoFilterUseCase", "command: $command")
        Log.d(FFMPEG_TAG, "start execute filter")
        when (val rc = FFmpeg.execute(command)) {
            RETURN_CODE_SUCCESS -> Log.i(Config.TAG, "Command execution completed successfully.")
            RETURN_CODE_CANCEL -> Log.i(Config.TAG, "Command execution cancelled by user.")
            else -> Log.i(Config.TAG, "Command execution failed with $rc and the output below.")
        }
        Log.d(FFMPEG_TAG, "finish execute filter")
    }
}