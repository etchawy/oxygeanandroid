package com.ellorem.oxygean.video.toolkits.videofilter

import android.net.Uri
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.daasuu.epf.EPlayerView
import com.ellorem.oxygean.video.R
import com.ellorem.oxygean.video.toolkits.SelectedTool
import com.ellorem.oxygean.video.toolkits.ToolKitFragment
import com.ellorem.oxygean.video.util.Constants.EXTENSION_MP4
import com.ellorem.oxygean.video.util.Constants.PREFIX_FILTERED
import com.ellorem.oxygean.video.util.Constants.WITH_TEXT_STICKER
import com.ellorem.oxygean.video.util.MediaUtil
import com.ellorem.oxygean.video.videodata.*
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.block_filter.*
import kotlinx.android.synthetic.main.block_leftside_toolkit.*
import kotlinx.android.synthetic.main.fragment_edit.*
import java.io.File
import java.util.*

abstract class FilterFragmentDecorator : Fragment() {

    companion object {
        const val TEXT_STICKER_EXTENSION = ".png"
        const val PLAY_NEXT = -1
        const val REPEAT = -2
    }

    protected var resultVideos = mutableListOf<EditVideoData>()

    protected var selectedTool: SelectedTool = SelectedTool.CutSplitSelected
    var currentVideo: EditVideoData? = null

    var timer: Timer? = null
    var screenWidth: Int = 0

    private var selectedFilter: VideoFilter? = null
    private var player: SimpleExoPlayer? = null
    private var playerView: EPlayerView? = null
    private val videoFilterUseCase = VideoFilterUseCase()
    private val imageOverlayUseCase = ImageOverlayUseCase()
    private val filterAndImageOverlayUseCase = FilterAndImageOverlayUseCase()
    private lateinit var filterTool: FilterTool
    private var stepSelected = true

    private fun stepsOrAllToggleButtonLogic() {

        val stepButton = step_button
        val allButton = all_button
        allButton.background = null
        stepButton.setOnClickListener {
            allButton.background = null
            stepButton.setBackgroundResource(R.drawable.border)
            stepSelected = true
            tempRemoveFilterForVideo()
        }
        allButton.setOnClickListener {
            stepButton.background = null
            allButton.setBackgroundResource(R.drawable.border)
            stepSelected = false
            saveTempFilter(selectedFilter ?: VideoFilter.ORIGINAL)
        }
    }

    protected fun initListOfFilters() {
        filterTool = FilterTool(
                filters_preview_recycler,
                title_filter_text,
                requireContext(),
                screenWidth
        ).apply {
            initToolSettings { videoFilter -> setFilter(videoFilter) }
        }
        stepsOrAllToggleButtonLogic()
    }

    private fun initListOfFiltersForCurrentVideo() {
        currentVideo?.let {
            filterTool.setNewVideo(this, it)
        }
    }

    protected fun initVideoPlayer() {
        player = ExoPlayerFactory.newSimpleInstance(context)
        setGlPlayerView()
    }

    fun setRepeatMode() {
        player?.repeatMode = Player.REPEAT_MODE_ONE
    }

    fun provideWidthCurrentVideo(): Int? = player?.videoFormat?.width

    protected fun startPlayVideo() {
        val playNextMode: (() -> Unit)? =
                when (selectedTool) {
                    SelectedTool.CutSplitSelected -> {
                        { playNextVideo() }
                    }
                    else -> null
                }
        currentVideo?.video?.videoURL?.let {
            player?.apply {
                prepare(createMediaSourceFromURL(it))
                playNextMode?.let {
                    player?.addListener(object : Player.EventListener {
                        override fun onPlayerStateChanged(playWhenReady: Boolean, state: Int) {
                            if (state == ExoPlayer.STATE_ENDED) {
                                player?.removeListener(this)
                                it.invoke()
                            }
                        }
                    })
                }
                repeatMode = if (playNextMode == null) Player.REPEAT_MODE_ONE else Player.REPEAT_MODE_OFF
                playWhenReady = true
            }
            addFilterOnView()
        }
    }

    protected fun stopPlayVideo() {
        player?.stop()
        timer?.cancel()
    }

    protected fun restartPlayVideo() {
        stopPlayVideo()
        playNextVideo(REPEAT)
    }

    abstract fun playNextVideo(position: Int = PLAY_NEXT)

    private fun addFilterOnView() {
        currentVideo?.videoFilterAccepted?.let {
            when (selectedTool) {
                SelectedTool.FiltersToolSelected -> {
                    changePlayerViewFilter(
                            if (it.isTempApplied)
                                it.tempFilter
                            else
                                VideoFilter.ORIGINAL
                    )
                }
                else -> changePlayerViewFilter(
                        if (it.isApplied)
                            it.appliedFilter
                        else
                            VideoFilter.ORIGINAL
                )
            }
        }
    }

    private fun createMediaSourceFromURL(videoURL: String): MediaSource {
        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(context,
                Util.getUserAgent(context, context?.getString(R.string.app_name)))
        return ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(videoURL))
    }

    private fun setGlPlayerView() {
        playerView = EPlayerView(context)
        playerView?.setSimpleExoPlayer(player)
        playerView?.layoutParams = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layout_movie_wrapper.addView(playerView, 0)
        playerView?.onResume()
    }

    private fun setFilter(videoFilter: VideoFilter) {
        selectedFilter = videoFilter
        videoFilter.let {
            saveTempFilter(videoFilter)
            changePlayerViewFilter(videoFilter)
        }
    }

    private fun changePlayerViewFilter(filter: VideoFilter) {
        playerView?.setGlFilter(glFilterConverter(requireContext(), filter))
    }

    private fun saveTempFilter(videoFilter: VideoFilter) {
        fun VideoFilterAccepted.setTempFilter() {
            tempFilter = videoFilter
            isTempApplied = true
        }
        if (stepSelected) {
            resultVideos[resultVideos.indexOf(currentVideo)].videoFilterAccepted.apply {
                setTempFilter()
            }
        } else {
            resultVideos.forEach { it.videoFilterAccepted.setTempFilter() }
        }
    }

    protected fun initFilterTool() {
        initFilterButtons()
        initListOfFiltersForCurrentVideo()
        setSelectedPreviouslyFilter()
    }

    private fun initFilterButtons() {
        close_button.setOnClickListener {
            loading_layout.visibility = View.VISIBLE
            tempRemoveFilterForVideo()
            hideFilterEditor()
            loading_layout.visibility = View.GONE
        }

        check_button.setOnClickListener {
            loading_layout.visibility = View.VISIBLE
            tempApplyFilterForVideo()
            hideFilterEditor()
            loading_layout.visibility = View.GONE
        }
    }

    private fun setSelectedPreviouslyFilter() {
        filterTool.selectFilter(
                currentVideo?.videoFilterAccepted?.let {
                    if (it.isApplied) it.appliedFilter else VideoFilter.ORIGINAL
                } ?: VideoFilter.ORIGINAL)
    }

    private fun hideFilterEditor() {
        selectedTool = SelectedTool.CutSplitSelected
        edit_fragment_filter_block.visibility = View.GONE
        edit_fragment_cutting_block.visibility = View.VISIBLE
        left_toolkit_filter.setImageResource(R.drawable.ic_toolkit_filters_p)
        playNextVideo(REPEAT)
        updateListOfVideos()
    }

    abstract fun updateListOfVideos()

    private fun tempApplyFilterForVideo() {
        if (stepSelected) {
            resultVideos.forEach {
                with(it.videoFilterAccepted) {
                    when (tempFilter) {
                        VideoFilter.ORIGINAL -> {
                            isTempApplied = false
                            isApplied = false
                            appliedFilter = VideoFilter.ORIGINAL
                        }
                        else -> if (isTempApplied) {
                            isApplied = true
                            isTempApplied = false
                            appliedFilter = tempFilter
                        }
                    }
                }
            }
        } else {
            val filter = selectedFilter ?: VideoFilter.ORIGINAL
            resultVideos.forEach {
                with(it.videoFilterAccepted) {
                    when (filter) {
                        VideoFilter.ORIGINAL -> {
                            isTempApplied = false
                            isApplied = false
                            appliedFilter = VideoFilter.ORIGINAL
                        }
                        else -> {
                            isApplied = true
                            isTempApplied = false
                            tempFilter = filter
                            appliedFilter = filter
                        }
                    }
                }
            }
        }
    }

    private fun tempRemoveFilterForVideo() {
        resultVideos.forEach {
            with(it.videoFilterAccepted) {
                isTempApplied = false
                tempFilter = appliedFilter
            }
        }
    }

    protected suspend fun applyFilterForVideo() {
        resultVideos.forEach { video ->
            var resFile = MediaUtil.createFile(requireContext(), EXTENSION_MP4, PREFIX_FILTERED)
            if (video.videoFilterAccepted.isApplied && video.textSticker != null) {
                filterAndImageOverlayUseCase.addFilterAndImageToVideo(
                        File(video.video.videoURL),
                        resFile,
                        video.textSticker!!,
                        video.videoFilterAccepted.appliedFilter,
                        requireContext()
                )
                video.video.videoURL = resFile.absolutePath
            } else {
                with(video.videoFilterAccepted) {
                    if (isApplied) {
                        videoFilterUseCase.filterVideo(requireContext(), File(video.video.videoURL), resFile, video.videoFilterAccepted.appliedFilter)
                        video.video.videoURL = resFile.absolutePath
                    }
                }
                video.textSticker?.let { sticker ->
                    resFile = MediaUtil.createFile(requireContext(), EXTENSION_MP4, WITH_TEXT_STICKER)
                    imageOverlayUseCase.addImageToVideo(
                            File(video.video.videoURL),
                            resFile,
                            sticker.imageFile,
                            sticker.xP,
                            sticker.yP,
                            sticker.sizePX
                    )
                    video.video.videoURL = resFile.absolutePath
                }
            }
        }
    }

    protected fun setCurrentVideoItem(video: EditVideoData) {
        currentVideo = video
        val textSticker = currentVideo?.textSticker
        if (textSticker != null) {
            if (textSticker.isExist && textSticker.editedText.isNotEmpty()) {
                edit_users_story_text.apply {
                    x = textSticker.x
                    y = textSticker.y
                    layoutParams = FrameLayout
                            .LayoutParams(
                                    textSticker.width,
                                    textSticker.height)
                    rotation = textSticker.rotation
                    gravity = textSticker.gravity
                    setText(textSticker.editedText, TextView.BufferType.SPANNABLE)
                    isVisible = true
                }
            }
        } else {
            edit_users_story_text.text = ToolKitFragment.EMPTY_TEXT
            edit_users_story_text.isVisible = false
        }
        updateListOfVideosAdapter()
    }

    abstract fun updateListOfVideosAdapter()

/*    override fun onStop() {
        lifecycleScope.launch(Dispatchers.Default) {
            MediaUtil.clearPngFiles(requireContext())
        }
        super.onStop()
    }*/
}