package com.ellorem.oxygean.video.util

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.util.TypedValue
import android.util.TypedValue.COMPLEX_UNIT_DIP
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment


fun Fragment.showKeyboard() {
    activity?.currentFocus?.let {
        val imm = it.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(it, 0)
    }
}


fun Fragment.hideKeyboard() {
    view?.let {
        activity?.let {
            val inputMethodManager = it.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view?.windowToken, 0)
        }
    }
}

fun Int.dpToPx() = (this * Resources.getSystem().displayMetrics.density).toInt()