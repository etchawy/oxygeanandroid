package com.ellorem.oxygean.video.toolkits

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.text.SpannableString
import android.view.*
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.GestureDetectorCompat
import androidx.core.view.doOnPreDraw
import androidx.core.view.get
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.ellorem.oxygean.video.R
import com.ellorem.oxygean.video.toolkits.texteditor.TextEditor
import com.ellorem.oxygean.video.toolkits.texteditor.TextSticker
import com.ellorem.oxygean.video.toolkits.texteditor.ViewBitmapConverter
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.FontDescriptor
import com.ellorem.oxygean.video.toolkits.texteditor.scaledragtool.RotationGestureDetector
import com.ellorem.oxygean.video.toolkits.videofilter.FilterFragmentDecorator
import com.ellorem.oxygean.video.util.MediaUtil
import com.ellorem.oxygean.video.util.dpToPx
import com.ellorem.oxygean.video.util.hideKeyboard
import com.ellorem.oxygean.video.util.showKeyboard
import kotlinx.android.synthetic.main.block_font_styles_toolkit.*
import kotlinx.android.synthetic.main.block_leftside_toolkit.*
import kotlinx.android.synthetic.main.block_text_editor.*
import kotlinx.android.synthetic.main.fragment_edit.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


abstract class ToolKitFragment : FilterFragmentDecorator(), ToolsSwitcher {

    companion object {
        const val DEF_SCALE_INTO_TRASH_BIN = 3f
        const val X_POSITION_STICKER_APPEARANCE = 250f
        const val Y_POSITION_STICKER_APPEARANCE = 400f
        const val CLICKABLE_AREA_AROUND_THE_VIEW = 60
        const val EMPTY_TEXT = ""
    }

    var isTextEditorActivated = false

    private lateinit var textEditor: TextEditor
    private var isEditorCreated = false
    private var currentScale = 1f

    private val scaleListener =
            object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
                override fun onScale(detector: ScaleGestureDetector): Boolean {
                    currentScale *= detector.scaleFactor
                    edit_users_story_text.scaleX = currentScale
                    edit_users_story_text.scaleY = currentScale
                    return true
                }
            }

    private val rotationListener =
            object : RotationGestureDetector.OnRotationGestureListener {
                override fun onRotation(rotationDetector: RotationGestureDetector) {
                    edit_users_story_text.rotation = -rotationDetector.angle
                }
            }

    private val gestureListener =
            object : GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapUp(e: MotionEvent): Boolean {
                    var result = false
                    edit_users_story_text.doOnPreDraw {
                        val x = e.x
                        val y = e.y
                        val left = it.x - CLICKABLE_AREA_AROUND_THE_VIEW
                        val right = left + it.height + 2 * CLICKABLE_AREA_AROUND_THE_VIEW
                        val top = it.y - CLICKABLE_AREA_AROUND_THE_VIEW
                        val bottom = top + it.width + 2 * CLICKABLE_AREA_AROUND_THE_VIEW

                        val viewXRange = left..right
                        val viewYRange = top..bottom
                        if (viewXRange.contains(x) && viewYRange.contains(y)) {
                            turnOnTextManager()
                            result = true
                        }
                    }
                    return result
                }

                override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
                    edit_users_story_text.x -= distanceX
                    edit_users_story_text.y -= distanceY
                    return true
                }
            }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initLeftToolBar()
    }

    @SuppressLint("ClickableViewAccessibility")
    fun setOnTouchListenerFor(view: View?) {
        if (view != null) {
            val context = view.context
            //val scaleDetector = ScaleGestureDetector(context, scaleListener)
            val rotationDetector = RotationGestureDetector(rotationListener)
            val gestureDetector = GestureDetectorCompat(context, gestureListener)

            main_movement_area.setOnTouchListener { _, event ->
                showTrash()
                activateTrash(isInTrashArea(view))
                if (event.action == MotionEvent.ACTION_UP) {
                    rotationDetector.viewAngle = view.rotation
                    if (isInTrashArea(view)) {
                        if (view is TextView) {
                            currentScale *= scalingFactor
                            applyScale(view)
                            deleteTextSticker()
                        }
                        // todo for sticker
                    }
                    hideTrash()
                }
//                val scale = scaleDetector.onTouchEvent(event)
                val rotation = rotationDetector.onTouchEvent(event)
                val gesture = gestureDetector.onTouchEvent(event)
                gesture || rotation // add scale || at start of the line for add scale

            }
            view.setOnTouchListener { _, event ->
                gestureDetector.onTouchEvent(event)
            }
        } else {
            main_movement_area.setOnTouchListener(null)
        }
    }

    protected fun initLeftToolBar() {
        left_toolkit_text.setOnClickListener {
            if (selectedTool == SelectedTool.CutSplitSelected) {
                isTextEditorActivated = true
                selectedTool = SelectedTool.TextEditorSelected
                turnOnTextManager()
            }
        }
        left_toolkit_filter.setOnClickListener {
            if (selectedTool == SelectedTool.CutSplitSelected) {
                loading_layout.visibility = View.VISIBLE
                turnOffTextManager()
                selectedTool = SelectedTool.FiltersToolSelected
                turnOnFilterEditor()
                loading_layout.visibility = View.GONE
            }
        }
        left_toolkit_sticker.setOnClickListener {
            //todo later
        }
        left_toolkit_poll.setOnClickListener {
            //todo later
        }
    }

    override fun turnOnTextManager() {
        left_toolkit_text.setImageResource(R.drawable.ic_toolkit_text_a)

        //make video cycle
        setRepeatMode()
        textEditor = if (fontDescriptor != null) {
            TextEditor(edit_toolkit_center_text_editor, fontDescriptor)
        } else {
            edit_toolkit_center_text_editor.setText(EMPTY_TEXT)
            TextEditor(edit_toolkit_center_text_editor, currentVideo!!.textSticker?.textDescriptor)
        }
        isEditorCreated = true

        list_of_videos_copy.isVisible = false
        edit_users_story_text.isVisible = false
        edit_fragment_left_toolkit.isVisible = false
        edit_fragment_cutting_block.isVisible = false
        edit_fragment_center_text.isVisible = true
        navigation.isVisible = false

        edit_toolkit_center_text_editor.requestFocus()
        showKeyboard()

        //buttons for managing the text editor
        edit_toolkit_center_cancel.setOnClickListener {
            hideTextEditor()
            deleteTextSticker()
            turnOffTextManager()
        }
        edit_toolkit_center_done.setOnClickListener {
            provideTextStickerToVideoView()
        }
        edit_toolkit_layout.setOnClickListener {
            provideTextStickerToVideoView()
        }

        //button for the font style panel
        edit_toolkit_text_font_size_button.setOnClickListener {
            textEditor.changeFontSize()
            edit_toolkit_text_font_size_button.setImageResource(textEditor.fontSize.imageId)
        }
        edit_toolkit_text_font_type_button.setOnClickListener {
            textEditor.changeFontType()
            edit_toolkit_text_font_type_button.setImageResource(textEditor.fontType.imageId)
        }
        edit_toolkit_text_align_button.apply {
            setImageResource(textEditor.alignType.imageId)
            setOnClickListener {
                textEditor.changeAlignType()
                edit_toolkit_text_align_button.setImageResource(textEditor.alignType.imageId)
            }
        }
        edit_toolkit_textcolor_recycler.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = textEditor.colorAdapter
        }
        edit_toolkit_textfont_recycler.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = textEditor.fontAdapter
        }
    }

    var fontDescriptor: FontDescriptor? = null
    private fun provideTextStickerToVideoView() {
//        list_of_videos_copy.isVisible = true
        if (!textEditor.isEmpty()) {
            fontDescriptor = textEditor.getFontDescriptor()
            edit_users_story_text.apply {
                isVisible = true
                x = X_POSITION_STICKER_APPEARANCE
                y = Y_POSITION_STICKER_APPEARANCE
                layoutParams = FrameLayout
                        .LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT)
                gravity = textEditor.gravity
                setText(textEditor.provideSpannableText(), TextView.BufferType.SPANNABLE)
            }
            setOnTouchListenerFor(edit_users_story_text)
        }
        hideTextEditor()
        setNavButtonsForTextManager()
        countScalingFactor()
    }

    private fun setNavButtonsForTextManager() {
        edit_toolkit_text_cancel.apply {
            isVisible = true
            setOnClickListener {
                deleteTextSticker()
                turnOffTextManager()
            }
        }
        edit_toolkit_text_done.apply {
            isVisible = true
            setOnClickListener {
                turnOffTextManager()
            }
        }
    }

    private fun hideTextEditor() {
        edit_fragment_left_toolkit.isVisible = true
        edit_fragment_center_text.isVisible = false
        hideKeyboard()
    }

    override fun turnOffTextManager() {
        left_toolkit_text.setImageResource(R.drawable.ic_toolkit_text_p)
        setOnTouchListenerFor(null)
        list_of_videos_copy.isVisible = true
        edit_fragment_left_toolkit.isVisible = true
        edit_fragment_cutting_block.isVisible = true
        edit_fragment_center_text.isVisible = false
        edit_toolkit_text_done.isVisible = false
        edit_toolkit_text_cancel.isVisible = false

        if (edit_users_story_text.text.isNotEmpty()) {
            saveUserStorySticker(edit_users_story_text)
        }

        selectedTool = SelectedTool.CutSplitSelected
        isTextEditorActivated = false
        navigation.isVisible = true
    }

    private fun saveUserStorySticker(tSticker: TextView) {
        val file = MediaUtil.createPNG(requireContext(), TEXT_STICKER_EXTENSION)
        layout_movie_wrapper[0].doOnPreDraw {
            val sticker = TextSticker(
                    tSticker.x,
                    tSticker.y,
                    tSticker.width,
                    tSticker.height,
                    tSticker.rotation,
                    textEditor.getFontDescriptor(),
                    SpannableString(tSticker.text),
                    tSticker.gravity,
                    (tSticker.x - layout_movie_wrapper.left) * 1f / it.width,// check on <0
                    (tSticker.y - layout_movie_wrapper.top) * 1f / it.height,
                    provideStickerSizeInPx(tSticker),
                    true,
                    file
            )
            currentVideo?.let { video ->
                video.textSticker?.imageFile?.delete()
                video.textSticker = sticker
            }
            fontDescriptor = null
        }
        lifecycleScope.launch(Dispatchers.Default) {
            ViewBitmapConverter(file).createBitmapFromView(tSticker)
        }
    }

    private fun provideStickerSizeInPx(sticker: View): Int {
        provideWidthCurrentVideo()?.let {
            return it * (sticker.width) / (layout_movie_wrapper.right - layout_movie_wrapper.left)
        }
        return sticker.width.dpToPx()
    }

    private fun deleteTextSticker() {
        edit_users_story_text.text = EMPTY_TEXT
        edit_toolkit_center_text_editor.setText(EMPTY_TEXT)

        currentVideo!!.let {
            it.textSticker?.imageFile?.delete()
            it.textSticker = null
        }

        edit_users_story_text.isVisible = false
        fontDescriptor = null
        isTextAddOnReadyToDelete = false
        textEditor.clear()
        turnOffTextManager()
    }

    override fun turnOnFilterEditor() {
        edit_fragment_filter_block.visibility = View.VISIBLE
        edit_fragment_cutting_block.visibility = View.GONE
        left_toolkit_filter.setImageResource(R.drawable.ic_toolkit_filters_a)
        initFilterTool()
    }

    override fun turnOnPollEditor() {
        TODO("Not yet implemented")
    }

    override fun turnOnStickerEditor() {
        TODO("Not yet implemented")
    }

    override fun turnOffFilterEditor() {
        TODO("Not yet implemented")
    }

    override fun turnOffPollEditor() {
        TODO("Not yet implemented")
    }

    override fun turnOffStickerEditor() {
        TODO("Not yet implemented")
    }

    //trash container
    private fun isInTrashArea(view: View): Boolean {
        val vx = view.x + view.width / 2
        val vy = view.y + view.height / 2
        val tx1 = edit_trash.x
        val tx2 = edit_trash.x + edit_trash.width
        val ty1 = edit_trash.y
        val ty2 = edit_trash.y + edit_trash.height

        return (vx in tx1..tx2) && (vy in ty1..ty2)
    }

    private fun showTrash() {
        edit_toolkit_text_done.isVisible = false
        edit_toolkit_text_cancel.isVisible = false
        edit_fragment_left_toolkit.isVisible = false
        edit_trash.isVisible = true
    }

    private fun hideTrash() {
        edit_toolkit_text_done.isVisible = true
        edit_toolkit_text_cancel.isVisible = true
        edit_fragment_left_toolkit.isVisible = true
        edit_trash.isVisible = false
    }

    private fun countScalingFactor() {
        edit_users_story_text.doOnPreDraw { story ->
            scalingFactor = if (story.width > story.height) {
                (story.width.toDouble() / edit_trash.width.toDouble()).toFloat()
            } else {
                (story.height.toDouble() / edit_trash.height.toDouble()).toFloat()
            }
        }
    }

    private var scalingFactor: Float = DEF_SCALE_INTO_TRASH_BIN
    private var isTextAddOnReadyToDelete = false
    private fun activateTrash(isTrashHovered: Boolean) {
        if (isTrashHovered) {
            edit_trash.setColorFilter(ContextCompat.getColor(edit_trash.context, R.color.text_toolkit_color_brand_red))
            if (!isTextAddOnReadyToDelete) {
                isTextAddOnReadyToDelete = true
                currentScale /= scalingFactor
            }
        } else {
            edit_trash.setColorFilter(ContextCompat.getColor(edit_trash.context, R.color.text_toolkit_color_white))
            if (isTextAddOnReadyToDelete) {
                isTextAddOnReadyToDelete = false
                currentScale *= scalingFactor
            }
        }
        applyScale(edit_users_story_text)
    }

    private fun applyScale(view: View) {
        view.scaleX = currentScale
        view.scaleY = currentScale
    }
}