package com.ellorem.oxygean.video.videodata

import android.util.Log
import com.arthenica.mobileffmpeg.Config
import com.arthenica.mobileffmpeg.FFmpeg
import com.arthenica.mobileffmpeg.FFmpeg.RETURN_CODE_CANCEL
import com.arthenica.mobileffmpeg.FFmpeg.RETURN_CODE_SUCCESS
import java.io.File
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class ImageOverlayUseCase {

    suspend fun addImageToVideo(
            inVideo: File,
            outVideo: File,
            image: File,
            startX: Float,
            startY: Float,
            size: Int
    ): Unit = suspendCoroutine { it.resume(addImage(inVideo, outVideo, image, startX, startY, size)) }

    private fun addImage(inVideo: File, outVideo: File, sticker: File, startXp: Float, startYp: Float, size: Int) {
        val s = if (size % 2 == 0) size else size + 1
        val commands = arrayOf("-y",
                "-i", inVideo.absolutePath,
                "-vf", "movie=${sticker.absoluteFile} [watermark]; [in][watermark] overlay=$startXp*W:$startYp*H, scale=720:-2",
                "-vcodec", "libx264",
                "-preset:v", "ultrafast",
                "-profile:v", "high",
                "-pix_fmt", "yuv420p",
                "-crf:v", "30",
                outVideo.absolutePath
        )
        Log.d(FFMPEG_TAG, "start execute text")
        when (val rc = FFmpeg.execute(commands)) {
            RETURN_CODE_SUCCESS -> {
                Log.i(Config.TAG, "Command execution completed successfully.")
                Log.d("ImageOverlayUseCase", "image added to video")
            }
            RETURN_CODE_CANCEL -> {
                Log.i(Config.TAG, "Command execution cancelled by user.")
            }
            else -> {
                Log.i(Config.TAG, "Command execution failed with $rc and the output below.")
//                Config.printLastCommandOutput(Log.INFO)
                Log.e("ImageOverlayUseCase", "cannot add image to file")
            }
        }
        Log.d(FFMPEG_TAG, "finish execute text")
        inVideo.delete()
//        sticker.delete()
    }
}