package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font

import com.ellorem.oxygean.video.R

enum class AlignType(val id: Int, val imageId: Int) {
    CENTER(0, R.drawable.ic_align_center),
    LEFT(1, R.drawable.ic_align_left),
    RIGHT(2, R.drawable.ic_align_right);

    companion object {
        fun getTypeById(id: Int) = values()[id]
    }
}