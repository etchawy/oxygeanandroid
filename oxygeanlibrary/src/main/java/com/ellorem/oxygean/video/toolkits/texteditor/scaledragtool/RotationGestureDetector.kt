package com.ellorem.oxygean.video.toolkits.texteditor.scaledragtool

import android.view.MotionEvent
import kotlin.math.atan2

private const val INVALID_POINTER_ID = -1

class RotationGestureDetector(private val mListener: OnRotationGestureListener) {

    private var fX = 0f
    private var fY = 0f
    private var sX = 0f
    private var sY = 0f

    private var ptrID1: Int = INVALID_POINTER_ID
    private var ptrID2: Int = INVALID_POINTER_ID

    private var correction = 0f
    var viewAngle = 0f
    var angle = 0f
        private set

    fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> ptrID1 = event
                    .getPointerId(event.actionIndex)
            MotionEvent.ACTION_POINTER_DOWN -> {
                ptrID2 = event.getPointerId(event.actionIndex)
                sX = event.getX(event.findPointerIndex(ptrID1))
                sY = event.getY(event.findPointerIndex(ptrID1))
                fX = event.getX(event.findPointerIndex(ptrID2))
                fY = event.getY(event.findPointerIndex(ptrID2))
                correction = -viewAngle
            }
            MotionEvent.ACTION_MOVE -> if (ptrID1 != INVALID_POINTER_ID && ptrID2 != INVALID_POINTER_ID) {
                val nsX: Float = event.getX(event.findPointerIndex(ptrID1))
                val nsY: Float = event.getY(event.findPointerIndex(ptrID1))
                val nfX: Float = event.getX(event.findPointerIndex(ptrID2))
                val nfY: Float = event.getY(event.findPointerIndex(ptrID2))
                angle = correction + angleBetweenLines(fX, fY, sX, sY, nfX, nfY, nsX, nsY)
                mListener.onRotation(this)
            }
            MotionEvent.ACTION_UP -> {
                ptrID1 = INVALID_POINTER_ID
            }
            MotionEvent.ACTION_POINTER_UP -> ptrID2 = INVALID_POINTER_ID
            MotionEvent.ACTION_CANCEL -> {
                ptrID1 = INVALID_POINTER_ID
                ptrID2 = INVALID_POINTER_ID
            }
        }
        return true
    }

    private fun angleBetweenLines(
            fX: Float,
            fY: Float,
            sX: Float,
            sY: Float,
            nfX: Float,
            nfY: Float,
            nsX: Float,
            nsY: Float
    ): Float {
        val angle1 = atan2((fY - sY).toDouble(), (fX - sX).toDouble()).toFloat()
        val angle2 = atan2((nfY - nsY).toDouble(), (nfX - nsX).toDouble()).toFloat()
        var angle = Math.toDegrees(angle1 - angle2.toDouble()).toFloat() % 360
        if (angle < -180f) angle += 360.0f
        if (angle > 180f) angle -= 360.0f
        return angle
    }

    interface OnRotationGestureListener {
        fun onRotation(rotationDetector: RotationGestureDetector)
    }
}