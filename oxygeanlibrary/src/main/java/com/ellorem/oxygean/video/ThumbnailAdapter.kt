package com.ellorem.oxygean.video

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ellorem.oxygean.video.videodata.EditVideoData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlin.math.roundToInt


class ThumbnailAdapter(applicationContext: Context,
                       logos: List<EditVideoData>,
                       private val lifecycleOwner: LifecycleOwner,
                       private val lifecycleScope: LifecycleCoroutineScope
) : RecyclerView.Adapter<ThumbnailAdapter.ThumbnailViewHolder>(), ItemMoveCallbackListener.Listener {
    private var context: Context = applicationContext
    val videos = mutableListOf<EditVideoData>()
    private val inflater: LayoutInflater
    private val highlightedVideosPositions = mutableListOf<Int>()
    private val startDragListeners = mutableListOf<OnStartDragListener>()
    private var selectedItem: ThumbnailViewHolder? = null
    val selectedVideo: MutableLiveData<EditVideoData> = MutableLiveData()

    override fun getItemId(i: Int): Long {
        return 0
    }

    init {
        videos.addAll(logos)
        inflater = LayoutInflater.from(applicationContext)
    }

    fun addListener(listener: OnStartDragListener) {
        startDragListeners.add(listener)
    }

    fun removeListener(listener: OnStartDragListener) {
        startDragListeners.remove(listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThumbnailAdapter.ThumbnailViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ThumbnailViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return videos.size
    }

    override fun onBindViewHolder(holder: ThumbnailAdapter.ThumbnailViewHolder, position: Int) {
        holder.bind(position)

        holder.itemView.setOnTouchListener(fun(_: View, event: MotionEvent): Boolean {
            if (event.action == MotionEvent.ACTION_DOWN) {
                for (startDragListener in startDragListeners) {
                    startDragListener.onStartDrag(holder)
                }
            }
            return true
        })
    }

    fun setSelectedVideo(videoData: EditVideoData) {
        selectedVideo.postValue(videoData)
    }

    inner class ThumbnailViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
            RecyclerView.ViewHolder(inflater.inflate(R.layout.thumbnail_item, parent, false)) {

        private val icon: ImageView = itemView.findViewById(R.id.icon)
        val overlay: View = itemView.findViewById(R.id.overlay)
        private val duration: TextView = itemView.findViewById(R.id.duration)
        private var videoForPreview: EditVideoData? = null

        fun bind(position: Int) {
            videoForPreview = videos[position]
            val imageListener = MutableLiveData<Pair<Bitmap, Bitmap?>>()
            imageListener.observe(lifecycleOwner, Observer {
                Glide.with(itemView.context)
                        .load(it.second ?: it.first)
                        .into(icon)
            })

            lifecycleScope.launch(Dispatchers.Default) {
                val bitmapsPair = withContext(Dispatchers.Default) { getBitmaps(videoForPreview!!) }
                imageListener.postValue(bitmapsPair)
            }

            duration.text = String.format(context.getString(R.string.duration_format), videoForPreview!!.video.length.roundToInt())
            if (highlightedVideosPositions.contains(position)) {
                duration.setBackgroundColor(context.getColor(R.color.color_pink))
            } else {
                duration.setBackgroundColor(context.getColor(android.R.color.transparent))
            }

            if (videoForPreview == selectedVideo.value) {
                showSelectedItem(this)
            }
        }

        private suspend fun getBitmaps(videoForPreview: EditVideoData): Pair<Bitmap, Bitmap?> =
                suspendCoroutine { it.resume(getBitmapFromVideoAndAddFilter(context, videoForPreview)) }
    }

    override fun onRowMoved(fromPosition: Int, toPosition: Int) {
        if (startDragListeners.isEmpty()) {
            return
        }
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(videos, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(videos, i, i - 1)
            }
        }
        notifyItemMoved(fromPosition, toPosition)
        for (startDragListener in startDragListeners) {
            startDragListener.onDataReordered(fromPosition, toPosition)
        }
    }

    override fun onRowSelected(itemViewHolder: ThumbnailAdapter.ThumbnailViewHolder) {

    }

    override fun onRowClear(itemViewHolder: ThumbnailAdapter.ThumbnailViewHolder) {
    }

    fun swipeToDismiss(adapterPosition: Int) {
        if (startDragListeners.isEmpty() || adapterPosition < 0) {
            return
        }
        //do not remove last video
        if (videos.size > 1) {
            videos.removeAt(adapterPosition)
            notifyDataSetChanged()
            for (startDragListener in startDragListeners) {
                startDragListener.onDataRemoved(adapterPosition)
            }
        }
    }

    fun click(itemViewHolder: RecyclerView.ViewHolder) {
        if (startDragListeners.isEmpty()) {
            return
        }
        for (startDragListener in startDragListeners) {
            startDragListener.onClickItem(itemViewHolder.adapterPosition)
        }
        showSelectedItem(itemViewHolder)
    }

    private fun showSelectedItem(itemViewHolder: RecyclerView.ViewHolder) {
        if (selectedItem != null) {
            selectedItem!!.overlay.setBackgroundColor(context.getColor(R.color.color_semitransparent))
        }
        (itemViewHolder as ThumbnailViewHolder).overlay.background = null
        selectedItem = itemViewHolder
    }

    fun updateVideos(newVideos: List<EditVideoData>, withHighlight: Boolean) {
        highlightedVideosPositions.clear()
        if (withHighlight) {
            for (video in newVideos) {
                if (!videos.contains(video)) {
                    highlightedVideosPositions.add(newVideos.indexOf(video))
                }
            }
        }
        videos.clear()
        videos.addAll(newVideos)
        notifyDataSetChanged()
    }

    fun updateVideos(newVideos: List<EditVideoData>) {
        updateVideos(newVideos, false)
    }

    fun removeHighLight() {
        highlightedVideosPositions.clear()
        notifyDataSetChanged()
    }


    interface OnStartDragListener {

        fun onStartDrag(viewHolder: RecyclerView.ViewHolder)

        fun onClickItem(position: Int)

        fun onDataReordered(from: Int, to: Int)

        fun onDataRemoved(pos: Int)
    }
}

fun getBitmapFromVideoAndAddFilter(context: Context, videoForPreview: EditVideoData, options: RequestOptions? = null): Pair<Bitmap, Bitmap?> {
    val inputImage = getBitmapFromVideo(context, videoForPreview, options)
    val outputImage =
            with(videoForPreview.videoFilterAccepted) {
                if (isApplied) appliedFilter.imageFilter?.processFilter(inputImage)
                else inputImage
            }
    return Pair(inputImage, outputImage)
}

fun getBitmapFromVideo(context: Context, videoForPreview: EditVideoData, options: RequestOptions? = null): Bitmap {
    val requestBuilder = Glide
            .with(context)
            .asBitmap()
            .load(videoForPreview.video.videoURL)
    options?.let { requestBuilder.apply(options) }
    return requestBuilder.submit().get()
}