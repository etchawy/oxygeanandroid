package com.ellorem.oxygean.video.toolkits.videofilter

import android.graphics.Color
import android.media.ThumbnailUtils
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ellorem.oxygean.video.R
import com.ellorem.oxygean.video.util.MediaUtil


class FilterPreviewAdapter(private val videoForPreview: MediaUtil.VideoData,
                           private val selectFilterListener: SelectFilterListener
) : RecyclerView.Adapter<FilterPreviewAdapter.VideoViewHolder>() {
    private val videoFilterItemList = arrayListOf<VideoFilterItem>()

    fun updateData(list: List<VideoFilterItem>) {
        val diffResult = DiffUtil.calculateDiff(VideoFiltersDiffUtil(videoFilterItemList, list))
        setData(list)
        diffResult.dispatchUpdatesTo(this)
    }

    private fun setData(newData: List<VideoFilterItem>) {
        videoFilterItemList.clear()
        videoFilterItemList.addAll(newData)
    }

    inner class VideoViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
            RecyclerView.ViewHolder(inflater.inflate(R.layout.thumbnail_filter_preview_item, parent, false)) {

        private var thumbnailImage: ImageView? = null

        init {
            thumbnailImage = itemView.findViewById(R.id.icon)
        }

        fun bind(position: Int) {
            val filter = videoFilterItemList[position]
            thumbnailImage?.let {
                val inputImage = ThumbnailUtils.createVideoThumbnail(videoForPreview.videoURL, MediaStore.Video.Thumbnails.MICRO_KIND)
                val outputImage = filter.videoFilter.imageFilter?.processFilter(inputImage)
                Glide.with(it.context)
                        .load(outputImage ?: inputImage)
                        .into(it)
            }
            itemView.setBackgroundColor(if (filter.isSelected) Color.WHITE else Color.TRANSPARENT)
            itemView.setOnClickListener {
                selectFilterListener.selectFilter(filter.videoFilter)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return VideoViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int = videoFilterItemList.size

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        holder.bind(position)
    }

    fun selectItemAtIndex(position : Int){
        selectFilterListener.selectFilter(videoFilterItemList[position].videoFilter)
    }
}

interface SelectFilterListener {
    fun selectFilter(videoFilter: VideoFilter)
}

class VideoFiltersDiffUtil(private val oldList: List<VideoFilterItem>, private val newList: List<VideoFilterItem>) :
        DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].videoFilter.filterId == newList[newItemPosition].videoFilter.filterId

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].isSelected == newList[newItemPosition].isSelected
    }
}