package com.ellorem.oxygean.video.toolkits

sealed class SelectedTool {
    object CutSplitSelected : SelectedTool()
    object TextEditorSelected : SelectedTool()
    object FiltersToolSelected : SelectedTool()
}