package com.ellorem.oxygean.video.util;

import android.annotation.SuppressLint;
import android.util.Log;

import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.TrackMetaData;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.util.Matrix;

import java.io.IOException;
import java.lang.reflect.Field;

public class GetVideoRotationInDegrees {
    private final static String MP4_VIDEO_TAG = "vide";
    private final static Object EXTRACT_LOCKER = new Object();
    // disable public construction.
    GetVideoRotationInDegrees(){}

    @SuppressLint("Assert")
    public static Matrix getRotation(String filePath) throws IOException, IllegalArgumentException {
        Movie video;
        synchronized (EXTRACT_LOCKER){
            video = MovieCreator.build(filePath);
        }

        assert(video.getTracks().size() > 0);
        for(int i=0; i<video.getTracks().size();i++){
            Track videoTrack = video.getTracks().get(i);
            if(videoTrack.getHandler().equals(MP4_VIDEO_TAG)){
                TrackMetaData metaData = videoTrack.getTrackMetaData();
                return getMatrixFromReader(metaData.getMatrix(), metaData.getWidth(), metaData.getHeight());
            }
        }
        throw new IllegalArgumentException("Cannot find video track in target file.");
    }

    private static Matrix getMatrixFromReader(Matrix matrix, double width, double height) throws IllegalArgumentException{
        if(rotationEquals(matrix, Matrix.ROTATE_0)){
            Log.e("Rotation Was =", ""+Matrix.ROTATE_0);
            // using with and heigt to fix incorrect orientation on android
            if (width <= height){
                return Matrix.ROTATE_90;
            }
            return Matrix.ROTATE_0;
        }else if(rotationEquals(matrix, Matrix.ROTATE_90)){
            Log.e("Rotation Was =", ""+Matrix.ROTATE_90);
            return Matrix.ROTATE_90;
        }else if(rotationEquals(matrix, Matrix.ROTATE_180)){
            Log.e("Rotation Was =", ""+Matrix.ROTATE_180);
            return Matrix.ROTATE_180;
        }else if(rotationEquals(matrix, Matrix.ROTATE_270)){
            Log.e("Rotation Was =", ""+Matrix.ROTATE_270);
            return Matrix.ROTATE_270;
        }else{
            // The file did not have a rotate tag
            // On android, there will be no rotate tag added if recorded in landscape AFAICT..
            Log.e("Matrix -", "File did not have a rotate tag");
            return Matrix.ROTATE_0;
        }
    }

    private static boolean rotationEquals(Matrix source, Matrix target){
        try {
             MatrixReader sMatrixReader = new MatrixReader();
            return Double.compare(sMatrixReader.getA(source), sMatrixReader.getA(target)) == 0 && Double.compare(sMatrixReader.getB(source), sMatrixReader.getB(target)) == 0 && Double.compare(sMatrixReader.getC(source), sMatrixReader.getC(target)) == 0 && Double.compare(sMatrixReader.getD(source), sMatrixReader.getD(target)) == 0;
        }   catch(Exception ex){return false;}
    }

    private static class MatrixReader{
        final String FIELD_NAME_A = "a";
        final String FIELD_NAME_B = "b";
        final String FIELD_NAME_C = "c";
        final String FIELD_NAME_D = "d";

        private Field mField_A;
        private Field mField_B;
        private Field mField_C;
        private Field mField_D;

        MatrixReader(){
            try{
                mField_A = Matrix.class.getDeclaredField(FIELD_NAME_A);
                mField_A.setAccessible(true);
                mField_B = Matrix.class.getDeclaredField(FIELD_NAME_B);
                mField_B.setAccessible(true);
                mField_C = Matrix.class.getDeclaredField(FIELD_NAME_C);
                mField_C.setAccessible(true);
                mField_D = Matrix.class.getDeclaredField(FIELD_NAME_D);
                mField_D.setAccessible(true);
            }catch (NoSuchFieldException ex){
                // safe ignore
            }
        }
        double getA(Matrix m) throws IllegalAccessException{
            return (Double) mField_A.get(m);
        }
        double getB(Matrix m) throws IllegalAccessException{
            return (Double) mField_B.get(m);
        }
        double getC(Matrix m) throws IllegalAccessException{
            return (Double) mField_C.get(m);
        }
        double getD(Matrix m) throws IllegalAccessException{
            return (Double) mField_D.get(m);
        }
    }
}
