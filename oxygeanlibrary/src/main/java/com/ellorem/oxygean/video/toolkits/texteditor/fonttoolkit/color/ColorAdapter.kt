package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.color

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ellorem.oxygean.video.R

class ColorAdapter(private val listener: OnColorClickListener) :
        RecyclerView.Adapter<ColorIconViewHolder>() {

    private val listOfColor = listOf(
            Color(R.drawable.ic_color_container_white, R.color.text_toolkit_color_white),
            Color(R.drawable.ic_color_container_black, R.color.text_toolkit_color_black),
            Color(R.drawable.ic_color_container_brand_red, R.color.text_toolkit_color_brand_red),
            Color(R.drawable.ic_color_container_comms_style, R.color.text_toolkit_color_comms_style),
            Color(R.drawable.ic_color_container_comp_power, R.color.text_toolkit_color_computing_power),
            Color(R.drawable.ic_color_container_emo_intel, R.color.text_toolkit_color_emo_intelligence),
            Color(R.drawable.ic_color_container_fixing_flair, R.color.text_toolkit_color_fixing_flair),
            Color(R.drawable.ic_color_container_hack_knack, R.color.text_toolkit_color_hack_knack),
            Color(R.drawable.ic_color_container_knowledge, R.color.text_toolkit_color_knowledge_mode),
            Color(R.drawable.ic_color_container_my_clout, R.color.text_toolkit_color_my_clout),
            Color(R.drawable.ic_color_container_my_moxy, R.color.text_toolkit_color_my_moxy),
            Color(R.drawable.ic_color_container_purple, R.color.text_toolkit_color_purple),
            Color(R.drawable.ic_color_container_act_barom, R.color.text_toolkit_color_act_barometer)
    )

    private var switchedColor = listOfColor[0]

    fun getFirst(): Color = listOfColor[0]

    fun switchColor(color: Color) {
        switchedColor = color
        listOfColor.forEach { it.isSwitched = false }
        listOfColor.first { it.colorIconId == color.colorIconId }.isSwitched = true
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorIconViewHolder =
            ColorIconViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_color_in_editor, parent, false), listener)

    override fun onBindViewHolder(holder: ColorIconViewHolder, position: Int) =
            holder.bind(listOfColor[position])

    override fun getItemCount(): Int = listOfColor.size
}