package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.color

class Color(val colorIconId: Int, val colorId: Int, val secondColorId: Int = 0){
    var isSwitched = false
}