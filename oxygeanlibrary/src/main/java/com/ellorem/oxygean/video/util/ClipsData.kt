package com.ellorem.oxygean.video.util

import com.ellorem.oxygean.video.util.MediaUtil

data class ClipsData (var clips: MutableList<MediaUtil.VideoData>, var drafts: MutableList<String>) {
}