package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit

import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.color.Color
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font.AlignType
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font.Font
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font.FontSize
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font.FontType

data class FontDescriptor(
        val text: String,
        val fontSize: FontSize,
        val fontColor: Color,
        val fontType: FontType,
        val font: Font,
        val fontAlign: AlignType
)