package com.ellorem.oxygean.video.firstTimeMessages

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.os.Handler
import android.view.View
import android.view.ViewGroup
import com.ellorem.oxygean.video.R


class FirstTimeMessageCameraUseCase : BaseFirstTimeMessageUseCase() {

    private var view: View? = null
    private var message: View? = null
    private var parentView: ViewGroup? = null

    fun showMessage(context: Context, parentView: View?) {
        if (checkIsItFirstTime(context, FirstTimeMessageCase.CAMERA_CASE)) {
            if (parentView == null) return
            if (parentView !is ViewGroup) return
            this.parentView = parentView
            view = View.inflate(context, R.layout.block_first_time_message_camera, parentView)
            message = view?.findViewById(R.id.first_time_message_content)
        }
    }

    fun cleanMessage() {
        if (message != null && parentView != null) {
            Handler().post {
                message!!.animate()
                        .translationY(0f)
                        .alpha(0.0f)
                        .setListener(object : AnimatorListenerAdapter() {
                            override fun onAnimationEnd(animation: Animator?) {
                                super.onAnimationEnd(animation)
                                parentView!!.removeView(view)
                            }
                        })
            }
        }
    }
}