package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font

import com.ellorem.oxygean.video.R

enum class FontType(val id: Int, val imageId: Int) {
    DEFAULT(0, R.drawable.ic_font_bg_default),
    BACKGROUND(1, R.drawable.ic_font_bg_background2),
    BACKGROUND_REVERSE(2, R.drawable.ic_font_bg_background1),
    OUTLINED(3, R.drawable.ic_font_bg_border1),
    BORDERED(4, R.drawable.ic_font_bg_border2);


    companion object {
        fun getTypeById(id: Int) = values()[id]
    }
}