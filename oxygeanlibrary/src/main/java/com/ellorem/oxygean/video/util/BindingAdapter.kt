package com.ellorem.oxygean.video.util

import android.graphics.Color
import android.media.ThumbnailUtils
import android.provider.MediaStore
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.ellorem.oxygean.video.toolkits.videofilter.VideoFilterItem

//("app:previewSelected")
fun selectFilterListener(view: View, filter: VideoFilterItem?) {
    filter?.let {
        view.setBackgroundColor(if (it.isSelected) Color.WHITE else Color.TRANSPARENT)
    }
}

/*
//("app:filterPreview", "app:videoPreview")
fun videoFilterPreview(icon: ImageView, filter: VideoFilterItem?, video: MediaUtil.VideoData?) {
    if (filter != null && video != null) {
        val inputImage = ThumbnailUtils.createVideoThumbnail(video.videoURL, MediaStore.Video.Thumbnails.MICRO_KIND)
        val outputImage = filter.videoFilter.imageFilter?.processFilter(inputImage)
        Glide.with(icon.context)
                .load(outputImage ?: inputImage)
                .into(icon)
    }
}*/
