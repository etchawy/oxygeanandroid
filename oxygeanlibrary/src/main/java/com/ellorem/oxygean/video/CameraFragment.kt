/*
 * Copyright 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ellorem.oxygean.video

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.SurfaceTexture
import android.graphics.Typeface
import android.hardware.camera2.*
import android.media.CamcorderProfile
import android.media.ImageReader
import android.media.MediaRecorder
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.util.Log
import android.util.Size
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.Surface
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.lifecycleScope
import com.arthenica.mobileffmpeg.Config
import com.arthenica.mobileffmpeg.FFmpeg
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.daasuu.camerarecorder.CameraRecordListener
import com.daasuu.camerarecorder.CameraRecorder
import com.daasuu.camerarecorder.CameraRecorderBuilder
import com.daasuu.camerarecorder.LensFacing
import com.ellorem.oxygean.video.camera.CameraVideoFragment
import com.ellorem.oxygean.video.firstTimeMessages.FirstTimeMessageCameraUseCase
import com.ellorem.oxygean.video.firstTimeMessages.FirstTimeMessageCase
import com.ellorem.oxygean.video.firstTimeMessages.FirstTimeToastUseCase
import com.ellorem.oxygean.video.camera.PortraitFrameLayout
import com.ellorem.oxygean.video.camera.SampleGLView
import com.ellorem.oxygean.video.util.ClipsData
import com.ellorem.oxygean.video.util.MediaUtil
import com.ellorem.oxygean.video.util.VideoParser
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_camera.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.util.*
import kotlin.concurrent.fixedRateTimer
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class CameraFragment : CameraVideoFragment(), CameraActivity.KeyListener {

    private val REQUEST_STORAGE = 101
    private var sampleGLView: SampleGLView? = null
    protected var cameraRecorder: CameraRecorder? = null
    private var filepath: String? = null
    private val recordBtn: TextView? = null
    protected var lensFacing = LensFacing.BACK
    protected var cameraWidth = 1280
    protected var cameraHeight = 720
    protected var videoWidth = 720
    protected var videoHeight = 720
    private var toggleClick = false

    private val dots = mutableListOf<View>()

    /** Detects, characterizes, and connects to a CameraDevice (used for all camera operations) */
    private val cameraManager: CameraManager by lazy {
        val context = requireContext().applicationContext
        context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
    }

    /** [CameraCharacteristics] corresponding to the provided Camera ID */
    private val characteristics: CameraCharacteristics by lazy {
        cameraManager.getCameraCharacteristics(cameraLensFacingDirection.toString())
    }

    /** File where the recording will be saved */
    private val pausedFiles = mutableListOf<File>()
    private val pausedFilesTimes = mutableListOf<Long>()

    //private val outputFileName = "/default.mp4"

    private val maxProgressTimeMillis: Long = 120000

    private var currentProgressTimeMillis: Long = 0
    private var oneVideoLenMilis: Long = -1

    private var currentProgress: Int = 0

    private var timerStepMilis: Long = 1200

    //private var currentTime: Long = 0

    var videos: MutableList<MediaUtil.VideoData> = mutableListOf()

    private var timer: Timer? = null

    private var recordingInProgress = false

    /** Readers used as buffers for camera still shots */
    private lateinit var imageReader: ImageReader

    private var inflater: LayoutInflater? = null
    private var firstNumberText: TextView? = null
    private var secondNumberText: TextView? = null
    private var thirdNumberText: TextView? = null

    private val firstTimeMessageCameraUseCase = FirstTimeMessageCameraUseCase()
    private val displayMetrics = DisplayMetrics()
    private var inactiveState = false
    private var inactiveTime = 0L

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        this.inflater = inflater
        return inflater.inflate(R.layout.fragment_camera, container, false)
    }

    fun getVideoFile(): File {
        return MediaUtil.createFile(requireContext(), "mp4")
    }

    @SuppressLint("MissingPermission")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        currentProgressTimeMillis = 0
        dots.clear()

        val typeface: Typeface? = ResourcesCompat.getFont(requireContext(), R.font.drukmediumapp)

        firstNumberText = requireView().findViewById(R.id.first_number)
        firstNumberText!!.typeface = typeface

        secondNumberText = requireView().findViewById(R.id.second_number)
        secondNumberText!!.typeface = typeface

        thirdNumberText = requireView().findViewById(R.id.third_number)
        thirdNumberText!!.typeface = typeface

        requireView().findViewById<View>(R.id.button_cancel).setOnClickListener {
            hideMessage()
        }
        videos = (requireActivity() as CameraActivity).videos
        if (videos.size > 0) {
            requireView().findViewById<ImageView>(R.id.close).visibility = View.VISIBLE
            setProgressNumering()
        }
        outputFile = null

        cameraLensFacingDirection = CameraCharacteristics.LENS_FACING_FRONT

        if (ActivityCompat.checkSelfPermission(requireContext(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            lifecycleScope.launch(Dispatchers.Main) {
                val thumb = MediaUtil.getLatestMediaAsync(requireContext())
                if (thumb != null) {
                    val options = RequestOptions()
                            .frame(0).override(40, 40)
                    Glide.with(requireContext()).asBitmap()
                            .load(thumb!!)
                            .apply(options)
                            .into(requireView().findViewById<ImageView>(R.id.upload_image))
                    requireView().findViewById<ImageView>(R.id.upload_image).background = requireContext().getDrawable(R.drawable.border)
                }
            }
        }

        requireView().findViewById<ImageView>(R.id.navigation).setOnClickListener {
            cancelFirstTimeMessage()
            showMessageOnBackPressed()
        }

        requireView().findViewById<ImageView>(R.id.check).setOnClickListener {
            cancelFirstTimeMessage()
            if (recordingInProgress || pausedFiles.size > 0) {
                var currRecordingTime = currentTimeOfRecording()
                if (currRecordingTime < resources.getInteger(R.integer.minimum_record_time)) {
                    FirstTimeToastUseCase.showMessage(requireContext(), plus, FirstTimeMessageCase.MINIMUM_RECORD, displayMetrics)
                    return@setOnClickListener
                }
            }
            requireView().findViewById<View>(R.id.loading_layout).visibility = View.VISIBLE
            requireView().findViewById<View>(R.id.main_top).visibility = View.GONE
            requireView().findViewById<View>(R.id.texture_view).visibility = View.GONE
            openEdit = true
            stopRecording()
        }

        if (videos.size > 0) {
            requireView().findViewById<ImageView>(R.id.close).visibility = View.GONE
            requireView().findViewById<ImageView>(R.id.check).visibility = View.VISIBLE
            requireView().findViewById<ImageView>(R.id.plus).visibility = View.VISIBLE
        }

        requireView().findViewById<ImageView>(R.id.close).setOnClickListener() {
            showMessage("Do you want to discard your recordings?", true, object : OnClickListenerInterface {
                override fun onClickYes() {
                    onDelete()
                    hideMessage()
                }

                override fun onClickNo() {
                    hideMessage()
                }

                override fun onClickExit() {
                    hideMessage()
                }
            })
        }
        // React to user touching the capture button
        requireView().findViewById<ImageView>(R.id.camera_record_start).setOnClickListener {
            firstTimeMessageCameraUseCase.cleanMessage()
            cancelFirstTimeMessage()
            recordingPressed()
        }

        requireView().findViewById<ImageView>(R.id.camera_record_stop).setOnClickListener {
            requireView().findViewById<ImageView>(R.id.camera_record_stop).visibility = View.GONE
            requireView().findViewById<ImageView>(R.id.camera_record_start).visibility = View.VISIBLE
            pauseRecording()
            requireView().findViewById<ImageView>(R.id.close).visibility = View.VISIBLE
            setWhiteDot()
            setRecordingInProgress(false)
            initFirstTimeMessages()
        }

        requireView().findViewById<View>(R.id.plus).setOnClickListener {
            cancelFirstTimeMessage()
            var currRecordingTime = currentTimeOfRecording()
            if (currRecordingTime < resources.getInteger(R.integer.minimum_record_time)) {
                FirstTimeToastUseCase.showMessage(requireContext(), plus, FirstTimeMessageCase.MINIMUM_RECORD, displayMetrics)
                return@setOnClickListener
            }

            requireView().findViewById<ImageView>(R.id.camera_record_stop).visibility =
                    View.GONE
            requireView().findViewById<ImageView>(R.id.camera_record_start).visibility =
                    View.VISIBLE
//                requireView().findViewById<View>(R.id.camera_switch).visibility = View.VISIBLE
            currentProgressTimeMillis = 0
            currentProgress = 0
            requireView().findViewById<ProgressBar>(R.id.progress_bar).setProgress(currentProgress)
            clearDotsOnProgressBar()
            stopRecording()
            isAddVideo = true
        }

        requireView().findViewById<View>(R.id.camera_flash).setOnClickListener { toggleFlashLight() }
        requireView().findViewById<View>(R.id.camera_switch).setOnClickListener {
            switchCamera()
            if (cameraLensFacingDirection == CameraCharacteristics.LENS_FACING_BACK) {
                requireView().findViewById<View>(R.id.camera_flash).visibility = View.GONE
            } else {
                requireView().findViewById<View>(R.id.camera_flash).visibility = View.VISIBLE
            }
        }

        requireView().findViewById<View>(R.id.upload).setOnClickListener {
            cancelFirstTimeMessage()
            if (ActivityCompat.checkSelfPermission(requireContext(),
                            Manifest.permission.READ_EXTERNAL_STORAGE) !== PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        REQUEST_STORAGE)
            } else {
                onUploadPressed()
            }

        }
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
    }

    private fun chooseVideoSize(choices: Array<Size>): Size? {
        for (size in choices) {
            if (1920 == size.width && 1080 == size.height) {
                return size
            }
        }
        for (size in choices) {
            if (size.width == size.height * 4 / 3 && size.width <= 1080) {
                return size
            }
        }
        Log.e("CameraFragment", "Couldn't find any suitable video size")
        return choices[choices.size - 1]
    }

    private fun chooseOptimalSize(choices: Array<Size>, width: Int, height: Int, aspectRatio: Size): Size? {
        // Collect the supported resolutions that are at least as big as the preview Surface
        val bigEnough: MutableList<Size> = ArrayList()
        val w = aspectRatio.width
        val h = aspectRatio.height
        for (option in choices) {
            if (option.height == option.width * h / w && option.width >= width && option.height >= height) {
                bigEnough.add(option)
            }
        }

        // Pick the smallest of those, assuming we found any
        return if (bigEnough.size > 0) {
            Collections.min(bigEnough, CompareSizesByArea())
        } else {
            Log.e("CameraFragment", "Couldn't find any suitable preview size")
            choices[0]
        }
    }


    private fun switchCamera(){
        releaseCamera()
        lensFacing = if (lensFacing == LensFacing.BACK) {
            LensFacing.FRONT
        } else {
            LensFacing.BACK
        }
        toggleClick = true
        if (cameraLensFacingDirection == CameraCharacteristics.LENS_FACING_BACK) {
            cameraLensFacingDirection = CameraCharacteristics.LENS_FACING_FRONT
        } else if (cameraLensFacingDirection == CameraCharacteristics.LENS_FACING_FRONT) {
            cameraLensFacingDirection = CameraCharacteristics.LENS_FACING_BACK
        }
        val flashView = requireView().findViewById<ImageView>(R.id.camera_flash)
        flash = CameraMetadata.FLASH_MODE_OFF
        flashView.setImageResource(R.mipmap.flash_off)
    }

    @SuppressLint("CutPasteId")
    private fun setUpCamera() {
        setUpCameraView()
        var mPreviewSize:Size? = null
        val manager = activity?.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            Log.d(TAG, "tryAcquire")
//            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
//                throw RuntimeException("Time out waiting to lock camera opening.")
//            }
            val cameraId = manager.cameraIdList[cameraLensFacingDirection]

            val characteristics = manager.getCameraCharacteristics(cameraId)
            val map = characteristics
                    .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
                    ?: throw RuntimeException("Cannot get available preview/video sizes")
            val mVideoSize = chooseVideoSize(map.getOutputSizes(MediaRecorder::class.java))
            val width = requireView().findViewById<PortraitFrameLayout>(R.id.texture_view).width
            val height = requireView().findViewById<PortraitFrameLayout>(R.id.texture_view).height
            mPreviewSize = mVideoSize?.let {
                chooseOptimalSize(map.getOutputSizes(SurfaceTexture::class.java),
                        width, height, it)

            }
            val orientation = resources.configuration.orientation

//            mPreviewSize?.let {
//                if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
//                    requireView().findViewById<PortraitFrameLayout>(R.id.texture_view).setAspectRatio(it.width, it.height)
//                } else {
//                    requireView().findViewById<PortraitFrameLayout>(R.id.texture_view).setAspectRatio(it.height, it.width)
//                }
//            }
        } catch (e: Exception){
            Log.d("CameraFragment", e.message)
        }
        val profile:CamcorderProfile? = getProfile()
        mPreviewSize?.let {
            profile?.let { pro->
                cameraRecorder = CameraRecorderBuilder(activity, sampleGLView) //.recordNoFilter(true)
                        .cameraRecordListener(object : CameraRecordListener {
                            override fun onGetFlashSupport(flashSupport: Boolean) {
                            }

                            override fun onRecordComplete() {
                                if (isStopRecording) {
                                    lifecycleScope.launch(Dispatchers.Default) {
                                        prepareEditAsync(openEdit)
                                    }
                                    isStopRecording = false
                                }
                            }

                            override fun onRecordStart() {}
                            override fun onError(exception: java.lang.Exception) {
                                Log.e("CameraRecorder", exception.toString())
                            }

                            override fun onCameraThreadFinish() {
                                if (toggleClick) {
                                    requireActivity().runOnUiThread { setUpCamera() }
                                }
                                toggleClick = false
                            }
                        })
//                        .videoSize(min(pro.videoFrameWidth,pro.videoFrameHeight), min(pro.videoFrameWidth,pro.videoFrameHeight))
                        .cameraSize(pro.videoFrameWidth * 2, pro.videoFrameHeight * 2)
                        .lensFacing(lensFacing)
                        .build()
            }
        }
    }

    @Throws(CameraAccessException::class)
    private fun getProfile(): CamcorderProfile? {
        var profile: CamcorderProfile? = null
        val manager = requireActivity().getSystemService(Context.CAMERA_SERVICE) as CameraManager
        val cameraId = manager.cameraIdList[cameraLensFacingDirection].toInt()
        if (CamcorderProfile.hasProfile(cameraId, CamcorderProfile.QUALITY_480P)) profile = CamcorderProfile.get(cameraId, CamcorderProfile.QUALITY_480P) else if (CamcorderProfile.hasProfile(cameraId, CamcorderProfile.QUALITY_HIGH)) profile = CamcorderProfile.get(cameraId, CamcorderProfile.QUALITY_HIGH) else if (CamcorderProfile.hasProfile(cameraId, CamcorderProfile.QUALITY_LOW)) profile = CamcorderProfile.get(cameraId, CamcorderProfile.QUALITY_LOW) else if (CamcorderProfile.hasProfile(cameraId, CamcorderProfile.QUALITY_480P)) profile = CamcorderProfile.get(cameraId, CamcorderProfile.QUALITY_480P) else if (CamcorderProfile.hasProfile(cameraId, CamcorderProfile.QUALITY_1080P)) profile = CamcorderProfile.get(cameraId, CamcorderProfile.QUALITY_1080P) else if (CamcorderProfile.hasProfile(cameraId, CamcorderProfile.QUALITY_720P)) profile = CamcorderProfile.get(cameraId, CamcorderProfile.QUALITY_720P)
        return profile
    }

    private fun setUpCameraView() {
        requireActivity().runOnUiThread {
            requireView().findViewById<PortraitFrameLayout>(R.id.texture_view).removeAllViews()
            sampleGLView = null
            sampleGLView = SampleGLView(requireContext())
            sampleGLView!!.setTouchListener { event, width, height ->
                if (cameraRecorder == null) return@setTouchListener
                cameraRecorder!!.changeManualFocusPoint(event.x, event.y, width, height)
            }
            cameraRecorder?.changeAutoFocus()
            requireView().findViewById<PortraitFrameLayout>(R.id.texture_view).addView(sampleGLView)
        }
    }

    private fun releaseCamera() {
        sampleGLView?.onPause()
        if (cameraRecorder != null) {
            cameraRecorder?.stop()
            cameraRecorder!!.release()
            cameraRecorder = null
        }
        if (sampleGLView != null) {
            requireView().findViewById<PortraitFrameLayout>(R.id.texture_view).removeView(sampleGLView)
            sampleGLView = null
        }
    }

    override fun onResume() {
        super.onResume()
        setUpCamera()
        openEdit=false
        isAddVideo = false
        isStopRecording = false
        //videos.clear()
    }

    private fun initFirstTimeMessages() {
        inactiveState = true
        inactiveTime = System.currentTimeMillis()
        fun checkTime(case: FirstTimeMessageCase): Boolean {
            if (inactiveState) {
                val currentTime = System.currentTimeMillis()
                val caseDelay = requireContext().resources.getInteger(case.delay)
                return currentTime - inactiveTime >= caseDelay
            }
            return false
        }
        FirstTimeToastUseCase.showMessage(requireContext(), plus, FirstTimeMessageCase.ADD_NEW_VIDEO_CASE_STOPPED_BY_USER, displayMetrics)
        FirstTimeToastUseCase.showMessageDelay(requireContext(), check, FirstTimeMessageCase.INACTIVITY_SHORT, displayMetrics) {
            return@showMessageDelay checkTime(FirstTimeMessageCase.INACTIVITY_SHORT)
        }
        FirstTimeToastUseCase.showMessageDelay(requireContext(), check, FirstTimeMessageCase.INACTIVITY_MEDIUM, displayMetrics) {
            return@showMessageDelay checkTime(FirstTimeMessageCase.INACTIVITY_MEDIUM)
        }
        FirstTimeToastUseCase.showMessageDelay(requireContext(), close, FirstTimeMessageCase.INACTIVITY_LONG, displayMetrics) {
            return@showMessageDelay checkTime(FirstTimeMessageCase.INACTIVITY_LONG)
        }
    }

    private fun cancelFirstTimeMessage() {
        FirstTimeToastUseCase.cleanMessage()
        inactiveState = false
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_STORAGE) {
            if (permissions[0] == Manifest.permission.READ_EXTERNAL_STORAGE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onUploadPressed()
            }
        }
    }

    private fun onUploadPressed() {
        if (recordingInProgress) {
            showMessage("Do you want to stop recording?", object : OnClickListenerInterface {
                override fun onClickYes() {
                    if (timer != null) {
                        timer!!.cancel()
                    }
                    stopRecording()
                    if (flash != CameraMetadata.FLASH_MODE_OFF) {
                        toggleFlashLight()
                    }
                    resetScreen()
                    (requireActivity() as CameraActivity).showUpload();
                }

                override fun onClickNo() {
                    hideMessage()
                }

                override fun onClickExit() {
                    hideMessage()
                }

            })
        } else {
            if (flash != CameraMetadata.FLASH_MODE_OFF) {
                toggleFlashLight()
            }
            resetScreen()
            (requireActivity() as CameraActivity).showUpload();
        }
    }

    private fun currentTimeOfRecording(): Long {
        var currRecordingTime = 0L;
        if (recordingInProgress && oneVideoLenMilis > 0) {
            currRecordingTime = System.currentTimeMillis() - oneVideoLenMilis;
        }
        if (pausedFilesTimes != null && pausedFilesTimes.size > 0) {
            for (time in pausedFilesTimes) {
                currRecordingTime += time
            }
        }
        return currRecordingTime
    }

    private fun setWhiteDot() {
        val progressView = requireView().findViewById<ProgressBar>(R.id.progress_bar)
        val dot: View = layoutInflater.inflate(R.layout.white_dot, null)
        val pixelsW: Float = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4F, resources.displayMetrics)
        val pixelsH: Float = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4F, resources.displayMetrics)

        val params = RelativeLayout.LayoutParams(pixelsW.toInt(), pixelsH.toInt())
        params.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.progress_bar)
        params.addRule(RelativeLayout.ALIGN_TOP, R.id.progress_bar)
        dot.x = progressView.x + progressView.width * currentProgress / 100
        dot.layoutParams = params
        dot.requestLayout()

        dots.add(dot)
        requireView().findViewById<RelativeLayout>(R.id.progress_bar_layout).addView(dot)
        requireView().findViewById<RelativeLayout>(R.id.progress_bar_layout).requestLayout()
    }

    private fun recordingPressed() {
        if (recordingInProgress) {
            resumeRecording()
        } else {
            startRecording()
        }
        oneVideoLenMilis = System.currentTimeMillis()
        requireView().findViewById<ImageView>(R.id.camera_record_start).visibility = View.GONE
        requireView().findViewById<ImageView>(R.id.camera_record_stop).visibility = View.VISIBLE

        requireView().findViewById<ImageView>(R.id.close).visibility = View.VISIBLE
        requireView().findViewById<ImageView>(R.id.check).visibility = View.VISIBLE
        requireView().findViewById<View>(R.id.camera_switch).visibility = View.GONE
        requireView().findViewById<View>(R.id.camera_flash).visibility = View.GONE

        requireView().findViewById<ImageView>(R.id.plus).visibility = View.VISIBLE
        requireView().findViewById<View>(R.id.upload).visibility = View.GONE
    }

    private fun onDelete() {
        if (recordingInProgress) {
            requireView().findViewById<ImageView>(R.id.camera_record_stop).visibility =
                    View.GONE
            requireView().findViewById<ImageView>(R.id.camera_record_start).visibility =
                    View.VISIBLE
            requireView().findViewById<View>(R.id.camera_switch).visibility = View.VISIBLE
            outputFile = null
            // because we set output file to bnull
            Log.d(TAG, "Recording stopped. Output file: $outputFile ")
//            try {
//                stopRecordingVideo()
////                mMediaRecorder!!.stop()
////                mMediaRecorder!!.release()
//            } catch (e: Throwable) {
//                Log.e("CameraFragment", "Stop recorder failed " + e.message)
//            }
            timer!!.cancel()
            pausedFiles.clear()
            pausedFilesTimes.clear()
            stopRecording()
            currentProgressTimeMillis = 0
            currentProgress = 0
            requireView().findViewById<ProgressBar>(R.id.progress_bar).progress = currentProgress
            clearDotsOnProgressBar()
        } else {

            if (videos.size > 0) {
                val lastVideo = videos.get(videos.size - 1)
                File(lastVideo.videoURL).delete()
                videos.remove(lastVideo)

            }

            //update numbering at progress bar
            setProgressNumering()
        }

        requireView().findViewById<View>(R.id.upload).visibility = View.VISIBLE
        if (videos.size == 0) {
            requireView().findViewById<ImageView>(R.id.close).visibility = View.GONE
            requireView().findViewById<ImageView>(R.id.check).visibility = View.GONE
            requireView().findViewById<ImageView>(R.id.plus).visibility = View.GONE

        }

    }


    private fun setProgressNumering() {
        firstNumberText!!.setTextColor(Color.WHITE)
        firstNumberText!!.visibility = View.VISIBLE
        secondNumberText!!.visibility = View.VISIBLE
        thirdNumberText!!.visibility = View.VISIBLE
        if (videos.size == 0) {
            firstNumberText!!.visibility = View.GONE
            secondNumberText!!.visibility = View.GONE
            thirdNumberText!!.visibility = View.GONE
        } else if (videos.size == 1) {
            firstNumberText!!.text = "1"
            secondNumberText!!.visibility = View.GONE
            thirdNumberText!!.visibility = View.GONE
        } else if (videos.size == 2) {
            firstNumberText!!.text = "1"
            secondNumberText!!.text = "2"
            thirdNumberText!!.visibility = View.GONE
        } else {
            firstNumberText!!.text = (videos.size - 2).toString()
            firstNumberText!!.setTextColor(requireContext().getColor(R.color.color_semitransparent_white))
            secondNumberText!!.text = (videos.size - 1).toString()
            thirdNumberText!!.text = (videos.size).toString()
        }
    }

    private fun resetScreen() {
        requireView().findViewById<ImageView>(R.id.close).visibility = View.GONE
        requireView().findViewById<ImageView>(R.id.check).visibility = View.GONE
        requireView().findViewById<ImageView>(R.id.plus).visibility = View.GONE
        requireView().findViewById<ImageView>(R.id.camera_record_stop).visibility = View.GONE
        requireView().findViewById<View>(R.id.camera_switch).visibility = View.VISIBLE
        requireView().findViewById<ImageView>(R.id.camera_record_start).visibility = View.VISIBLE
        requireView().findViewById<View>(R.id.camera_switch).visibility = View.VISIBLE
        requireView().findViewById<View>(R.id.upload).visibility = View.VISIBLE

        firstNumberText!!.visibility = View.GONE
        secondNumberText!!.visibility = View.GONE
        thirdNumberText!!.visibility = View.GONE

        currentProgressTimeMillis = 0
        currentProgress = 0

        requireView().findViewById<ProgressBar>(R.id.progress_bar).progress = currentProgress
        clearDotsOnProgressBar()

    }

    private suspend fun prepareEditAsync(isEdit: Boolean): Boolean? = suspendCoroutine { cont ->
        var isDone = prepareEditSync(isEdit)
        cont.resume(isDone)
    }

    private fun prepareEditSync(isEdit: Boolean):Boolean {
        Thread.sleep(2000)
        if (pausedFiles != null && pausedFiles.size > 0) {

            val resFile = MediaUtil.createFile(requireContext(), "mp4", "merged")
            VideoParser().mergeVideos(pausedFiles, resFile)
            var timeOfVideo = 0L
            for (time in pausedFilesTimes) {
                timeOfVideo += time
            }
            val length = resFile!!.length()
            val thumbFile = MediaUtil.createFile(requireContext(), "jpeg")
            MediaUtil.createThumbFile(requireContext(), resFile!!, thumbFile)
            var needToFlip = false
//            if (cameraLensFacingDirection == CameraCharacteristics.LENS_FACING_BACK) {
//                needToFlip = true;
//            }
            val video = MediaUtil.VideoData(
                    timeOfVideo / 1000.0,
                    listOf(),
                    0,
                    true,
                    thumbFile.absolutePath,
                    length,
                    resFile.absolutePath,
                    false,
                    "",
                    needToFlip
            )
            videos.add(video)
            for (file in pausedFiles) {
                file.delete()
            }
            pausedFiles.clear()
            pausedFilesTimes.clear()
        }

        outputFile == null

        Log.e("DebugCameraFragment", "stop recording finished")
        if(isEdit){
            openEdit=false
            startEditScreen()
        }
        if(isAddVideo){
            if (videos.size > 0) {
                requireActivity().runOnUiThread { requireView().findViewById<ImageView>(R.id.check).isEnabled = true }
            }
            requireActivity().runOnUiThread {
                requireView().findViewById<ImageView>(R.id.close).visibility = View.VISIBLE
                setProgressNumering()
                requireView().findViewById<View>(R.id.upload).visibility = View.VISIBLE
            }
            isAddVideo = false
        }
        return true
    }

    private var isAddVideo = false
    private var openEdit = false
    private var isStopRecording = false

    private fun stopRecording() {
        Log.e("DebugCameraFragment", "stopRecording")
        if (recordingInProgress && outputFile != null) {
            setRecordingInProgress(false)
            Log.e("DebugCameraFragment", "recorder stopped")
            Log.d(TAG, "Recording stopped. Output file: $outputFile ")
            try {
                isStopRecording = true
                cameraRecorder?.stop()

                pausedFiles.add(outputFile!!)
                pausedFilesTimes.add(System.currentTimeMillis() - oneVideoLenMilis)
            } catch (e: Throwable) {
                Log.e("CameraFragment", "Stop recorder failed " + e.message)
            }
            timer!!.cancel()
        } else {
            setRecordingInProgress(false)
            lifecycleScope.launch(Dispatchers.Default) {
                prepareEditAsync(openEdit)
            }
            isStopRecording=false
        }
    }

    private fun pauseRecording() {
        Log.e("DebugCameraFragment", "pauseRecording")
        Log.d(TAG, "Recording paused")
        if (!recordingInProgress) {
            return
        }
        try {
//            stopRecordingVideo()
            cameraRecorder?.stop()

            pausedFiles.add(outputFile!!)
            pausedFilesTimes.add(System.currentTimeMillis() - oneVideoLenMilis)
        } catch (e: Throwable) {
            Log.e("CameraFragment", "Stop recorder failed " + e.message)
        }

        oneVideoLenMilis = -1L;
        timer!!.cancel()
        outputFile = null
        Log.e("DebugCameraFragment", "pause recording finished")
    }

    private fun resumeRecording() {
        Log.d(TAG, "Recording resumed")
        setWhiteDot()
        startRecording()
    }

    private fun startRecording() = lifecycleScope.launch(Dispatchers.Main) {
        Log.e("DebugCameraFragment", "startRecording")
        setRecordingInProgress(true)

        Log.e("DebugCameraFragment", "started recorder")
//        startRecordingVideo()
        outputFile = getVideoFile()
        cameraRecorder!!.start(outputFile.absolutePath)
        Log.e("DebugCameraFragment", "create session")

        timer = fixedRateTimer(period = timerStepMilis) {
            currentProgressTimeMillis += timerStepMilis
            Log.e("DebugCameraFragment", "recording time $currentProgressTimeMillis")

            requireActivity().runOnUiThread {
                requireView().findViewById<ProgressBar>(R.id.progress_bar).progress = currentProgress++
                if (currentProgressTimeMillis == maxProgressTimeMillis) {
                    stopRecording()
                    requireView().findViewById<ImageView>(R.id.camera_record_stop).visibility =
                            View.GONE
                    requireView().findViewById<ImageView>(R.id.camera_record_start).visibility =
                            View.VISIBLE

                    requireView().findViewById<ImageView>(R.id.close).visibility = View.VISIBLE
                    setProgressNumering()

                    FirstTimeToastUseCase.showMessage(requireContext(), plus, FirstTimeMessageCase.ADD_NEW_VIDEO_CASE_AUTO_STOP, displayMetrics)

                    initFirstTimeMessages()
                }
            }
        }
        Log.d(TAG, "Recording started")
    }

    private fun startEditScreen() {
        if (flash != CameraMetadata.FLASH_MODE_OFF) {
            toggleFlashLight()
        }
        flipVideosIfNeededAndFinish()
    }

    private fun flipVideosIfNeededAndFinish() {
//        GeneralUtils.checkForPermissionsMAndAbove(requireActivity(), true)
        lifecycleScope.launch(Dispatchers.Default) {
            for (video in videos) {
                if (video.needToFlip) {
                    val oldVideoFile = File(video.videoURL)
                    val resFile = MediaUtil.createFile(requireContext(), "mp4", "FLIPPED_")
                    val parser = VideoParser()
                    parser.rotateOrFlipVideoAsync(oldVideoFile, resFile, requireContext(), true)
                    video.needToFlip=false
                    oldVideoFile.delete()
                    video.videoURL = resFile.absolutePath
                    val thumbFile = MediaUtil.createFile(requireContext(), "jpeg")
                    MediaUtil.createThumbFile(requireContext(), resFile!!, thumbFile)
                    val oldImageFile = File(video.thumbURL);
                    oldImageFile.delete()
                    video.thumbURL = thumbFile.absolutePath
                }
            }
            requireActivity().runOnUiThread {
                resetScreen()
                (requireActivity() as CameraActivity).startEditScreen()
                requireView().findViewById<View>(R.id.main_top).visibility = View.VISIBLE
                requireView().findViewById<View>(R.id.texture_view).visibility = View.VISIBLE
                requireView().findViewById<View>(R.id.loading_layout).visibility = View.GONE
            }
        }
    }

    private fun setRecordingInProgress(inProgress: Boolean) {
        recordingInProgress = inProgress
        if (inProgress) {
            requireView().findViewById<ImageView>(R.id.camera_flash).visibility = View.GONE
            requireView().findViewById<ImageView>(R.id.camera_switch).visibility = View.GONE
        } else {
            if (cameraLensFacingDirection == CameraCharacteristics.LENS_FACING_BACK) {
                // Front camera
                requireView().findViewById<View>(R.id.camera_flash).visibility = View.GONE
            } else {
                // Back camera
                requireView().findViewById<View>(R.id.camera_flash).visibility = View.VISIBLE
            }
//            requireView().findViewById<ImageView>(R.id.camera_switch).visibility=View.VISIBLE
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun toggleFlashLight() {
        val flashView = requireView().findViewById<ImageView>(R.id.camera_flash)
        flash =
                if (flash == CameraMetadata.FLASH_MODE_OFF) {
                    flashView.setImageResource(R.mipmap.flash)
                    CameraMetadata.FLASH_MODE_TORCH
                } else {
                    flashView.setImageResource(R.mipmap.flash_off)
                    CameraMetadata.FLASH_MODE_OFF
                }
        try {
//            setupFlash(flash)
            oneVideoLenMilis = System.currentTimeMillis()

        } catch (e: Exception) {
            e.printStackTrace()
        }
        cameraRecorder?.let {
            if(it.isFlashSupport){
                it.switchFlashMode()
                it.changeAutoFocus()
            }
        }
    }

    /**
     * Creates a [CameraCaptureSession] and returns the configured session (as the result of the
     * suspend coroutine)
     */
    private suspend fun createCaptureSession(
            device: CameraDevice,
            targets: List<Surface>,
            handler: Handler? = null
    ): CameraCaptureSession = suspendCoroutine { cont ->

        // Creates a capture session using the predefined targets, and defines a session state
        // callback which resumes the coroutine once the session is configured
        device.createCaptureSession(targets, object : CameraCaptureSession.StateCallback() {

            override fun onConfigured(session: CameraCaptureSession) = cont.resume(session)

            override fun onConfigureFailed(session: CameraCaptureSession) {
                Log.e(TAG, "Camera session configuration failed but continue")
                cont.resume(session)
            }
        }, handler)
    }

    override fun onPause() {
        firstTimeMessageCameraUseCase.cleanMessage()
        super.onPause()
    }

    override fun onStart() {
        super.onStart()
        (requireActivity() as CameraActivity).addListener(this)
        firstTimeMessageCameraUseCase.showMessage(requireContext(), requireView())
        requireView().setOnClickListener {
            firstTimeMessageCameraUseCase.cleanMessage()
        }
    }


    override fun onStop() {
        resetUserActivity()
        (requireActivity() as CameraActivity).removeListener(this)
        super.onStop()
        releaseCamera()
        openEdit=false
        isAddVideo = false
        isStopRecording = false
    }

    private fun resetUserActivity() {
        if (recordingInProgress && outputFile != null) {
            Log.e("DebugCameraFragment", "recorder stopped")
            Log.d(TAG, "Recording stopped. Output file: $outputFile ")
            try {
//                mMediaRecorder!!.stop()
//                mMediaRecorder!!.release()
                cameraRecorder?.stop()
                pausedFiles.clear()
            } catch (e: Throwable) {
                Log.e("CameraFragment", "Stop recorder failed " + e.message)
            }
            timer!!.cancel()
        }

        if (flash != CameraMetadata.FLASH_MODE_OFF) {
            toggleFlashLight()
        }
        resetScreen()
//        try {
//            camera.close()
//        } catch (exc: Throwable) {
//            Log.e(TAG, "Error closing camera", exc)
//        }
    }

    override fun onDestroy() {
        super.onDestroy()
        releaseCamera()

//        if (mMediaRecorder != null) {
//            mMediaRecorder!!.release()
//        }
    }

//    override fun getTextureResource(): Int {
//        return R.id.texture_view
//    }

    override fun setUp(view: View?) {}

    companion object {
        private val TAG = CameraFragment::class.java.simpleName
        private const val RECORDER_VIDEO_BITRATE: Int = 10_000_000
    }

    override fun onBackPressed() {
        showMessageOnBackPressed()

    }

    fun showMessageOnBackPressed() {
        if (recordingInProgress || videos.size > 0) {
            showMessage("", true, "Start over", object : OnClickListenerInterface {
                override fun onClickYes() {
                    if (timer != null) {
                        timer!!.cancel()
                    }
                    stopRecording()
                    if (flash != CameraMetadata.FLASH_MODE_OFF) {
                        toggleFlashLight()
                    }
                    for (video in videos) {
                        File(video.videoURL).delete()
                    }
                    videos.clear()
                    clearDotsOnProgressBar()
                    setProgressNumering()

                    requireView().findViewById<ImageView>(R.id.close).visibility = View.GONE
                    requireView().findViewById<ImageView>(R.id.check).visibility = View.GONE
                    requireView().findViewById<ImageView>(R.id.plus).visibility = View.GONE
                    requireView().findViewById<ImageView>(R.id.camera_record_stop).visibility = View.GONE
                    requireView().findViewById<View>(R.id.camera_switch).visibility = View.VISIBLE
                    requireView().findViewById<ImageView>(R.id.camera_record_start).visibility = View.VISIBLE
                    requireView().findViewById<View>(R.id.camera_switch).visibility = View.VISIBLE
                    requireView().findViewById<View>(R.id.upload).visibility = View.VISIBLE

                    currentProgressTimeMillis = 0
                    currentProgress = 0
                    requireView().findViewById<ProgressBar>(R.id.progress_bar).progress = currentProgress
                    clearDotsOnProgressBar()

                    hideMessage()
                    //(requireActivity() as CameraActivity).onBackPresseSuper()
                }

                override fun onClickNo() {
                    hideMessage()
                }

                override fun onClickExit() {
                    if (timer != null) {
                        timer!!.cancel()
                    }
                    stopRecording()
                    if (flash != CameraMetadata.FLASH_MODE_OFF) {
                        toggleFlashLight()
                    }
                    for (video in videos) {
                        File(video.videoURL).delete()
                    }
                    videos.clear()
                    onBackSendEmptyJson()
                }

            })
        } else {
            onBackSendEmptyJson()
        }
    }

    private fun clearDotsOnProgressBar() {
        for (dot in dots) {
            requireView().findViewById<RelativeLayout>(R.id.progress_bar_layout).removeView(dot)
        }
        dots.clear()
    }

    fun onBackSendEmptyJson() {
        val clipsData = ClipsData(mutableListOf(), mutableListOf())
        val clipsJson = Gson().toJson(clipsData)
        (requireActivity() as CameraActivity).returnResult(clipsJson);
    }

    private fun showMessage(desc: String, withExit: Boolean, onClickListenerInterface: OnClickListenerInterface) {
        showMessage(desc, withExit, "Continue", onClickListenerInterface)
    }

    private fun showMessage(desc: String, withExit: Boolean, yesText: String, onClickListenerInterface: OnClickListenerInterface) {
        requireView().findViewById<TextView>(R.id.button_yes_text).text = yesText
        requireView().findViewById<RelativeLayout>(R.id.message).visibility = View.VISIBLE
        requireView().findViewById<TextView>(R.id.message_desc).text = desc
        requireView().findViewById<View>(R.id.button_cancel).setOnClickListener { onClickListenerInterface.onClickNo() }
        requireView().findViewById<View>(R.id.button_yes).setOnClickListener { onClickListenerInterface.onClickYes() }
        requireView().findViewById<View>(R.id.button_exit).setOnClickListener { onClickListenerInterface.onClickExit() }
        if (withExit) {
            requireView().findViewById<View>(R.id.button_exit).visibility = View.VISIBLE
        } else {
            requireView().findViewById<View>(R.id.button_exit).visibility = View.GONE
        }

    }

    private fun showMessage(desc: String, onClickListenerInterface: OnClickListenerInterface) {
        showMessage(desc, false, onClickListenerInterface);
    }

    fun hideMessage() {
        requireView().findViewById<RelativeLayout>(R.id.message).visibility = View.GONE
    }

    interface OnClickListenerInterface {
        fun onClickYes()
        fun onClickNo()
        fun onClickExit()
    }

    internal class CompareSizesByArea : Comparator<Size> {
        override fun compare(lhs: Size, rhs: Size): Int {
            // We cast here to ensure the multiplications won't overflow
            return java.lang.Long.signum(lhs.width.toLong() * lhs.height -
                    rhs.width.toLong() * rhs.height)
        }
    }
}