package com.ellorem.oxygean.video.toolkits.texteditor

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Matrix
import android.view.View
import androidx.core.graphics.scaleMatrix
import com.ellorem.oxygean.video.util.MediaUtil
import java.io.File
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class ViewBitmapConverter(private val file: File) {

    suspend fun createBitmapFromView(view: View): Bitmap =
            suspendCoroutine {
                it.resume(loadBitmapFromView(view))
            }

    private fun loadBitmapFromView(view: View): Bitmap {
        val adjMatrix = Matrix().apply {
            postRotate(view.rotation)
            scaleMatrix(view.scaleX, view.scaleY)
        }
        val viewBitmap = view.bitmap
        val adjusted = Bitmap.createBitmap(viewBitmap, 0, 0, viewBitmap.width, viewBitmap.height, adjMatrix, true)
        viewBitmap.recycle()
        MediaUtil.writeIntoStorage(file, adjusted)
        return adjusted
    }

    private val View.bitmap: Bitmap
        get() {
            val viewBitmap = Bitmap.createBitmap(viewWidth, viewHeight, Bitmap.Config.ARGB_8888)
            val viewCanvas = Canvas(viewBitmap)
            layout(left, top, right, bottom)
            draw(viewCanvas)
            return viewBitmap
        }

    private val View.viewWidth: Int
        get() = if (layoutParams.width > 0) layoutParams.width else width

    private val View.viewHeight: Int
        get() = if (layoutParams.height > 0) layoutParams.height else height
}
