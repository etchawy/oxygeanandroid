package com.ellorem.oxygean.video.toolkits

interface ToolsSwitcher {

    fun turnOnTextManager()
    fun turnOffTextManager()
    fun turnOnFilterEditor()
    fun turnOffFilterEditor()
    fun turnOnPollEditor()
    fun turnOffPollEditor()
    fun turnOnStickerEditor()
    fun turnOffStickerEditor()
}