package com.ellorem.oxygean.video.firstTimeMessages

import android.content.Context
import android.os.Handler
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.doOnPreDraw
import com.ellorem.oxygean.video.R


object FirstTimeToastUseCase : BaseFirstTimeMessageUseCase() {

    private var toast: Toast? = null
    private val handler = Handler()

    fun showMessage(context: Context, viewToAttach: View, case: FirstTimeMessageCase, displayMetrics: DisplayMetrics) {
        if (checkIsItFirstTime(context, case)) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val layout: View = inflater.inflate(R.layout.toast_first_time_message, null)

            val text = layout.findViewById<TextView>(R.id.toast_text)
            text.setText(case.textId)
            toast?.cancel()
            toast = Toast(context.applicationContext)

            val screenWidth = displayMetrics.widthPixels
            val screenHeight = displayMetrics.heightPixels

            viewToAttach.doOnPreDraw {
                val location = IntArray(2)
                viewToAttach.getLocationOnScreen(location)
                val xOfs = screenWidth - location[0] - MARGIN_X
                val yOfs = screenHeight - location[1] + viewToAttach.height
                toast?.let {
                    it.setGravity(Gravity.BOTTOM or Gravity.END, xOfs, yOfs)
                    it.duration = if (case == FirstTimeMessageCase.INACTIVITY_LONG) Toast.LENGTH_LONG else Toast.LENGTH_SHORT
                    it.view = layout
                    it.show()
                }
            }
        }
    }

    fun cleanMessage() {
        toast?.cancel()
    }

    fun showMessageDelay(context: Context,
                         viewToAttach: View,
                         case: FirstTimeMessageCase,
                         displayMetrics: DisplayMetrics,
                         checkCondition: () -> Boolean) {
        val timeDelay = context.resources.getInteger(case.delay).toLong()
        handler.postDelayed({
            if (checkCondition.invoke()) {
                showMessage(context, viewToAttach, case, displayMetrics)
            }
        }, timeDelay)
    }

    private const val MARGIN_X = 30
    private const val MARGIN_Y = 30
}