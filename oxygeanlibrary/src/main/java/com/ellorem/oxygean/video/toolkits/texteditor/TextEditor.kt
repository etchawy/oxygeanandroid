package com.ellorem.oxygean.video.toolkits.texteditor

import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.BackgroundColorSpan
import android.text.style.ForegroundColorSpan
import android.view.Gravity
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.core.text.clearSpans
import androidx.core.view.doOnPreDraw
import com.ellorem.oxygean.video.R
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.FontDescriptor
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.color.Color
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.color.ColorAdapter
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.color.OnColorClickListener
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font.*
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font.spans.FontSpan
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font.spans.OutlinedSpan
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.reduce
import kotlinx.android.synthetic.main.block_font_styles_toolkit.*

class TextEditor(private val editor: EditText, descriptor: FontDescriptor? = null) {

    var fontType = FontType.DEFAULT
    var fontSize = FontSize.MEDIUM
    var alignType = AlignType.CENTER
    var gravity = Gravity.CENTER

    val colorAdapter = ColorAdapter(object : OnColorClickListener {
        override fun onClick(color: Color) {
            changeColor(color)
        }
    })

    val fontAdapter = FontAdapter(object : OnFontClickListener {
        override fun onClick(font: Font) {
            changeFont(font)
        }
    }, editor.context)

    private var primaryColorId: Int = R.color.text_toolkit_color_white
    private var secondaryColorId: Int = R.color.text_toolkit_color_black

    //make it enums
    private var font = fontAdapter.getFirst()
    private var color = colorAdapter.getFirst()

    init {
        editor.gravity = Gravity.CENTER
        descriptor?.let { applyDescriptor(it) }
        editor.typeface = font.font
        editor.textSize = fontSize.value
        changeFont(font)
        changeColor(color)
        editor.addTextChangedListener( object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                fontAutoSizing()
            }
        })
    }

    private fun applyDescriptor(descriptor: FontDescriptor){
        editor.setText(descriptor.text)
        fontType = descriptor.fontType
        fontSize = descriptor.fontSize
        alignType = descriptor.fontAlign
        font = descriptor.font
        color = descriptor.fontColor
        applyAlign()
    }

    private fun fontAutoSizing(){
        editor.doOnPreDraw {
            if ((editor.layout.height > editor.height) && (fontSize != FontSize.SMALL)) {
                fontSize = editor.reduce(fontSize)
                editor.textSize = fontSize.value
                fontAutoSizing()
            }
        }
    }

    fun changeFontSize() {
        fontSize = if (fontSize.ordinal == FontSize.values().size - 1) {
            FontSize.getSizeById(0)
        } else {
            FontSize.getSizeById(fontSize.id + 1)
        }
        editor.textSize = fontSize.value
        setFontType()
    }

    fun changeFontType() {
        fontType = if (fontType.id == FontType.values().size - 1) {
            FontType.getTypeById(0)
        } else {
            FontType.getTypeById(fontType.id + 1)
        }
        setFontType()
    }

    fun changeAlignType(align: AlignType? = null) {
        alignType = align
                ?: if (alignType.id == AlignType.values().size - 1) {
                    AlignType.getTypeById(0)
                } else {
                    AlignType.getTypeById(alignType.id + 1)
                }
        applyAlign()
    }

    private fun applyAlign(){
        editor.gravity = when (alignType) {
            AlignType.LEFT -> Gravity.CENTER_VERTICAL xor Gravity.START
            AlignType.RIGHT -> Gravity.CENTER_VERTICAL xor Gravity.END
            else -> Gravity.CENTER
        }
        gravity = editor.gravity
    }

    fun changeFont(font: Font) {
        this.font = font
        fontAdapter.switchFont(font)
        editor.typeface = font.font
    }

    fun changeColor(color: Color) {
        this.color = color
        colorAdapter.switchColor(color)
        primaryColorId = color.colorId
        defineSecondaryColor(color.colorId)
        editor.setTextColor(ContextCompat.getColor(editor.context, color.colorId))
        setFontType()
    }

    private fun defineSecondaryColor(colorId: Int) {
        secondaryColorId = when (colorId) {
            R.color.text_toolkit_color_white -> R.color.text_toolkit_color_black
            R.color.text_toolkit_color_act_barometer -> R.color.text_toolkit_color_white
            R.color.text_toolkit_color_black -> R.color.text_toolkit_color_white
            R.color.text_toolkit_color_purple -> R.color.text_toolkit_color_white
            R.color.text_toolkit_color_my_moxy -> R.color.text_toolkit_color_white
            R.color.text_toolkit_color_my_clout -> R.color.text_toolkit_color_white
            R.color.text_toolkit_color_knowledge_mode -> R.color.text_toolkit_color_white
            R.color.text_toolkit_color_hack_knack -> R.color.text_toolkit_color_white
            R.color.text_toolkit_color_fixing_flair -> R.color.text_toolkit_color_white
            R.color.text_toolkit_color_emo_intelligence -> R.color.text_toolkit_color_white
            R.color.text_toolkit_color_computing_power -> R.color.text_toolkit_color_white
            R.color.text_toolkit_color_comms_style -> R.color.text_toolkit_color_white
            R.color.text_toolkit_color_brand_red -> R.color.text_toolkit_color_white
            else -> R.color.text_toolkit_color_white
        }
    }

    private fun setFontType() {
        when (fontType) {
            FontType.DEFAULT -> clearFontType()
            FontType.BACKGROUND -> setBackgroundSpan(false)
            FontType.BACKGROUND_REVERSE -> setBackgroundSpan(true)
            FontType.BORDERED -> setBorder(false)
            FontType.OUTLINED -> setBorder(true)
        }
        editor.setSelection(editor.text.length)
    }

    private fun clearFontType() {
        editor.setText(editor.text.toString().trim())
    }

    private fun setBackgroundSpan(isReverse: Boolean) {
        SpannableString(editor.text).apply {
            clearSpans()
            setSpan(ForegroundColorSpan(ContextCompat.getColor(editor.context, if (isReverse) secondaryColorId else primaryColorId)),
                    0, this.length,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            setSpan(BackgroundColorSpan(ContextCompat.getColor(editor.context, if (isReverse) primaryColorId else secondaryColorId)),
                    0, this.length,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            editor.setText(this)
        }
    }

    private fun setBorder(isReverse: Boolean) {
        SpannableString(editor.text).apply {
            clearSpans()
            setSpan(OutlinedSpan(
                    ContextCompat.getColor(editor.context, if (isReverse) secondaryColorId else primaryColorId),
                    ContextCompat.getColor(editor.context, if (isReverse) primaryColorId else secondaryColorId),
                    fontSize.borderWidth), 0, this.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            editor.setText(this)
        }
    }

    fun provideSpannableText(): SpannableString =
            SpannableString(editor.text).apply {
                setSpan(FontSpan(font.font, fontSize.value * 2),
                        0, this.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                if (fontType == FontType.DEFAULT) {
                    setSpan(ForegroundColorSpan(ContextCompat.getColor(editor.context, primaryColorId)),
                            0, this.length,
                            Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                }
            }

    fun getFontDescriptor(): FontDescriptor {
        val text = editor.text.toString()
        return FontDescriptor(text, fontSize, color, fontType, font, alignType)
    }

    fun isEmpty(): Boolean = editor.text.isNullOrEmpty()

    fun clear() {
        editor.clearComposingText()
    }
}