package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.color

interface OnColorClickListener {
    fun onClick(color: Color)
}