package com.ellorem.oxygean.video;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.WindowManager;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.ellorem.oxygean.video.util.ClipsData;
import com.ellorem.oxygean.video.util.MediaUtil;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class CameraActivity extends AppCompatActivity {

    static {
        System.loadLibrary("NativeImageProcessor");
    }

    public static final String DATA_INPUT = "com.ellorem.oxygean.video.CameraActivity.DataInput";
    public static final String DATA_OUTPUT = "com.ellorem.oxygean.video.CameraActivity.DataOutput";

    private List<MediaUtil.VideoData> videos = new ArrayList<>();
    private List<KeyListener> keyListeners = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        String data = getIntent().getStringExtra(DATA_INPUT);
        if (TextUtils.isEmpty(data)) {
            addFragment(new CameraFragment(), "CameraFragment");
        } else {
            ClipsData clips = new Gson().fromJson(data, ClipsData.class);
            List<MediaUtil.VideoData> allVideos = clips.getClips();
            if (allVideos == null || allVideos.size() == 0) {
                addFragment(new CameraFragment(), "CameraFragment");
                return;
            }
            List<MediaUtil.VideoData> justOriginals = new ArrayList<>();
            for (MediaUtil.VideoData videoData : allVideos) {
                if (videoData.isOriginal()) {
                    justOriginals.add(videoData);
                }
            }
            this.videos = justOriginals;
            startEditScreen();
        }
    }

    private void replaceFragment(Fragment destFragment, String tag, boolean removePrevious) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_layout, destFragment, tag);
        if (removePrevious) {
            fragmentManager.popBackStack();
        }
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void addFragment(Fragment destFragment, String tag) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_layout, destFragment, tag);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void showUpload() {
        replaceFragment(new UploadFragment(), "UploadFragment", false);
    }

    public void startEditScreen() {
        replaceFragment(new EditFragment(), "EditFragment", false);
    }

    public void startCameraScreen(List<MediaUtil.VideoData> newVideos) {
        if (this.videos == null) {
            this.videos = new ArrayList<>();
        }
        this.videos.addAll(newVideos);
        replaceFragment(new CameraFragment(), "CameraFragment", false);
    }

    public List<MediaUtil.VideoData> getVideos() {
        return videos;
    }

    public void addListener(KeyListener keyListener) {
        keyListeners.add(keyListener);
    }

    public void removeListener(KeyListener keyListener) {
        keyListeners.remove(keyListener);
    }

    @Override
    public void onBackPressed() {
        for (KeyListener keyListener : keyListeners) {
            keyListener.onBackPressed();
        }
        //super.onBackPressed();
    }

    public void returnResult(String json) {
        Intent intent = new Intent();
        intent.putExtra(DATA_OUTPUT, json);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    public void removeCurrentFragment() {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack();
        if (fragmentManager.findFragmentByTag("CameraFragment") == null) {
            addFragment(new CameraFragment(), "CameraFragment");
        }
        fragmentTransaction.commit();
    }

    interface KeyListener {
        void onBackPressed();
    }
}