package com.ellorem.oxygean.video

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ellorem.oxygean.video.util.MediaUtil
import com.ellorem.oxygean.video.videodata.EditVideoData
import com.ellorem.oxygean.video.toolkits.videofilter.VideoFilter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.io.File
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class ThumbnailsVideoPreviewAdapter(val videoForPreview: EditVideoData,
                                    private val lifecycleOwner: LifecycleOwner,
                                    private val lifecycleScope: LifecycleCoroutineScope
) : RecyclerView.Adapter<ThumbnailsVideoPreviewAdapter.VideoViewHolder>() {
    private val video: MediaUtil.VideoData = videoForPreview.video
    val filter = videoForPreview.videoFilterAccepted.appliedFilter

    inner class VideoViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
            RecyclerView.ViewHolder(inflater.inflate(R.layout.thumbnail_video_preview_item, parent, false)) {
        private var thumbnailImage: ImageView? = null

        init {
            thumbnailImage = itemView.findViewById(R.id.icon)
        }

        fun bind(position: Int) {
            if (videoForPreview.videoFilterAccepted.appliedFilter != VideoFilter.ORIGINAL) {
                val imageListener = MutableLiveData<Pair<Bitmap, Bitmap?>>()
                imageListener.observe(lifecycleOwner, Observer {
                    Glide.with(itemView.context)
                            .load(it.second ?: it.first)
                            .into(thumbnailImage!!)
                })

                lifecycleScope.launch(Dispatchers.Default){
                    val bitmapsPair = withContext(Dispatchers.Default) { getBitmaps(position) }
                    imageListener.postValue(bitmapsPair)
                }

            } else {
                Glide.with(itemView.context).asBitmap()
                        .load(video.videoURL)
                        .apply(prepareOptions(position))
                        .into(thumbnailImage!!)
            }
        }

        private suspend fun getBitmaps(position: Int): Pair<Bitmap, Bitmap?> = suspendCoroutine { it.resume(getBitmapFromVideo(position)) }

        private fun getBitmapFromVideo(position: Int): Pair<Bitmap, Bitmap?> {
            return getBitmapFromVideoAndAddFilter(itemView.context, videoForPreview, prepareOptions(position))
        }

        private fun prepareOptions(position: Int): RequestOptions {
            val intervalMillis = ((video.length / LIST_SIZE) * 1000000 * position).toLong()
            return RequestOptions().frame(intervalMillis).override(50, 70)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return VideoViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int = LIST_SIZE

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        holder.bind(position)
    }

    companion object {
        private const val LIST_SIZE = 15
    }
}