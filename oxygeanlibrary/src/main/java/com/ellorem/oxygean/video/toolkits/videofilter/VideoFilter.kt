package com.ellorem.oxygean.video.toolkits.videofilter

import android.content.Context
import android.util.Log
import com.daasuu.epf.filter.GlFilter
import com.daasuu.epf.filter.GlGrayScaleFilter
import com.daasuu.epf.filter.GlToneCurveFilter
import com.ellorem.oxygean.video.R
import com.ellorem.oxygean.video.videodata.CustomImageFilters
import com.zomato.photofilters.imageprocessors.Filter
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


enum class VideoFilter(val filterId: Int, val filterTitleId: Int, val filterACVFileId: Int? = null, val imageFilter: Filter? = null) {
    ORIGINAL(0, R.string.filter_original),
    MONO(1, R.string.filter_mono, imageFilter = CustomImageFilters.imageMonoFilter),
    COOL(2, R.string.filter_cool, R.raw.cool, imageFilter = CustomImageFilters.imageCoolFilter),
    CLARITY(3, R.string.filter_clarity, R.raw.clerity, imageFilter = CustomImageFilters.imageClarityFilter),
    WARM(4, R.string.filter_warm, R.raw.warm, imageFilter = CustomImageFilters.imageWarmFilter)
}

fun glFilterConverter(context: Context, videoFilter: VideoFilter): GlFilter {
    return when (videoFilter) {
        VideoFilter.ORIGINAL -> GlFilter()
        VideoFilter.MONO -> GlGrayScaleFilter()
        else -> GlToneCurveFilter(context.resources.openRawResource(
                videoFilter.filterACVFileId ?: R.raw.cool))
    }
}

fun ffmpegFilterConvertForComplex(context: Context, videoFilter: VideoFilter): String? {
    return when (videoFilter) {
        VideoFilter.ORIGINAL -> null
        VideoFilter.MONO -> MONO_PROPERTIES_COMPLEX
        else -> {
            val acvFile = createACVFile(context, videoFilter)?.absolutePath
            "$USE_CURVE_PROPERTIES_COMPLEX$acvFile "
        }
    }
}
fun ffmpegFilterConvert(context: Context, videoFilter: VideoFilter): String? {
    return when (videoFilter) {
        VideoFilter.ORIGINAL -> null
        VideoFilter.MONO -> MONO_PROPERTIES
        else -> {
            val acvFile = createACVFile(context, videoFilter)?.absolutePath
            "$USE_CURVE_PROPERTIES$acvFile"
        }
    }
}

data class VideoFilterItem(val videoFilter: VideoFilter, val isSelected: Boolean)

fun initVideoFilterItemsList(selectedFilter: VideoFilter = VideoFilter.ORIGINAL): List<VideoFilterItem> {
    val videoFilterItemsList = ArrayList<VideoFilterItem>(VideoFilter.values().size)
    VideoFilter.values().map {
        videoFilterItemsList.add(VideoFilterItem(it, it.filterId == selectedFilter.filterId))
    }
    return videoFilterItemsList
}

 fun createACVFile(context: Context, filter: VideoFilter): File? {
    filter.filterACVFileId?.let {
        try {
            val inputStream = context.resources.openRawResource(it)
            val cascadeDir: File = context.getDir("files", Context.MODE_PRIVATE)
            val title = context.resources.getString(it).substringAfterLast('/')
            cascadeDir.list()?.let { files ->
                if (files.contains(title)) {
                    return cascadeDir.listFiles()?.get(files.indexOf(title))
                }
            }
            val tempFile = File(cascadeDir, title)
            val outputStream = FileOutputStream(tempFile)
            val buffer = ByteArray(4096)
            var bytesRead: Int
            while (inputStream.read(buffer).also { bytesRead = it } != -1) {
                outputStream.write(buffer, 0, bytesRead)
            }
            inputStream.close()
            outputStream.close()
            return tempFile
        } catch (e: IOException) {
            Log.i("VideoFilter", e.message ?: "Error while creating acv-file")
        }
    }
    return null
}

//private const val MONO_PROPERTIES = "eq=gamma=1.1:brightness=0.1:contrast=1.15, format=gray"
private const val MONO_PROPERTIES = "-vf hue=s=0"
private const val USE_CURVE_PROPERTIES = "-vf curves=psfile="

private const val MONO_PROPERTIES_COMPLEX = "hue=s=0"
const val USE_CURVE_PROPERTIES_COMPLEX = "curves=psfile="

private const val COOL_PROPERTIES = "eq=contrast=1.05:gamma=1.1:saturation=0.95, selectivecolor=correction_method=relative:neutrals=.16 .12 -.01"
private const val CLARITY_PROPERTIES = "eq=brightness=0.07:saturation=0.9:contrast=1.1, selectivecolor=correction_method=relative:neutrals=.13 .08 -.01 .03"
private const val CHILL_PROPERTIES = "eq=gamma=1.1:brightness=-0.1:contrast=1.0:saturation=0.9, selectivecolor=correction_method=relative:reds=.1 .1 .1 .1:blues=.3 .3 .3 .3:cyans=.2 .2 .2 .2"
private const val WARM_PROPERTIES = "eq=gamma=1.03:saturation=1.08:contrast=1.05, selectivecolor=correction_method=relative:neutrals=0 .1 .15 .1:whites=0 .13 -.15"
private const val GLOW_PROPERTIES = "eq=brightness=0.2:saturation=0.9, selectivecolor=correction_method=relative:reds=.1 .1 .1 .1:yellows=.1 .1 .1 .1"
private const val VIBE_PROPERTIES = "eq=brightness=-0.2:contrast=1.1:gamma=1.1:saturation=1.1, selectivecolor=correction_method=relative:reds=.1 .1 .1 .1:yellows=.1 .1 .1 .1"