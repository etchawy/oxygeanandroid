package com.ellorem.oxygean.video.toolkits.videofilter

import android.content.Context
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ellorem.oxygean.video.util.dpToPx
import com.ellorem.oxygean.video.videodata.EditVideoData


class FilterTool(private val listOfFiltersRecyclerView: RecyclerView,
                 private val titleFilterTextView: TextView,
                 private val context: Context,
                 private val screenWidth: Int
) : SelectFilterListener {

    private lateinit var filtersPreviewAdapter: FilterPreviewAdapter
    private var applyFilter: ((VideoFilter) -> Unit)? = null
    private val listOfFiltersItems = MutableLiveData<List<VideoFilterItem>>()

    fun initToolSettings(applyFilter: (VideoFilter) -> Unit) {
        this.applyFilter = applyFilter
        listOfFiltersRecyclerView.layoutManager = LinearLayoutManager(context).apply {
            orientation = LinearLayoutManager.HORIZONTAL
        }
        listOfFiltersRecyclerView.addItemDecoration(FilterPreviewItemDecorator(context, VideoFilter.values().size))
    }

    fun setNewVideo(viewLifecycleOwner: LifecycleOwner, currentVideo: EditVideoData){
        filtersPreviewAdapter = FilterPreviewAdapter(currentVideo.video, this)
        listOfFiltersRecyclerView.adapter = filtersPreviewAdapter
        listOfFiltersItems.observe(viewLifecycleOwner, Observer { list ->
            filtersPreviewAdapter.updateData(list)
        })
        listOfFiltersItems.postValue(initVideoFilterItemsList())
        listOfFiltersRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                recyclerView.post {
                    selectMiddleItem()
                }
            }
        })
    }

    private fun selectMiddleItem() {
        val layoutManager = listOfFiltersRecyclerView.layoutManager as LinearLayoutManager
        val firstVisibleIndex = layoutManager.findFirstVisibleItemPosition()
        val lastVisibleIndex = layoutManager.findLastVisibleItemPosition()
        val visibleIndexes = listOf(firstVisibleIndex..lastVisibleIndex).flatten()
        for (i in visibleIndexes) {
            val vh = listOfFiltersRecyclerView.findViewHolderForLayoutPosition(i)
            if (vh?.itemView == null) {
                continue
            }
            val location = IntArray(2)
            vh.itemView.getLocationOnScreen(location)
            val x = location[0]
            val halfWidth = vh.itemView.width * 0.1
            val rightSide = x + halfWidth
            val leftSide = x - halfWidth
            val isInMiddle = screenWidth * .5 in leftSide..rightSide
            if (isInMiddle) {
                filtersPreviewAdapter.selectItemAtIndex(i)
                return
            }
        }
    }

    override fun selectFilter(videoFilter: VideoFilter) {
        titleFilterTextView.text = context.getString(videoFilter.filterTitleId)
        listOfFiltersItems.postValue(initVideoFilterItemsList(videoFilter))
        shiftPreviewToCenter(videoFilter.filterId)
        applyFilter?.invoke(videoFilter)
    }

    private fun shiftPreviewToCenter(position: Int) {
        val layoutManager = listOfFiltersRecyclerView.layoutManager as LinearLayoutManager
        val displayMetrics = context.resources.displayMetrics
        val offset = (displayMetrics.widthPixels / 2) - (25).dpToPx()
        layoutManager.scrollToPositionWithOffset(position, offset)
    }
}