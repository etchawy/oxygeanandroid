package com.ellorem.oxygean.video.util

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.ellorem.oxygean.video.util.Constants.EXTENSION_PNG
import kotlinx.coroutines.suspendCancellableCoroutine
import java.io.*
import java.text.SimpleDateFormat
import java.util.*


class MediaUtil {

    data class VideoData(var length: Double, var pauses: List<Double>,
                         var order: Int, var isOriginal: Boolean, var thumbURL: String,
                         var fileSize: Long,
                         var videoURL: String,
                         var shouldReduceSize: Boolean = false,
                         var rotation: String = "",
                         var needToFlip: Boolean = false,
                         var mediaId: Long = 0)

    companion object {

        /** Creates a [File] named with the current date and time */
        fun createFile(context: Context, extension: String): File {
            val sdf = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS", Locale.US)
            return File(context.getExternalFilesDir(null), "VID_${sdf.format(Date())}.$extension")
        }

        fun createPngFile(context: Context, extension: String): File {
            val sdf = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS", Locale.US)
            val tmpDir = File("${context.getExternalFilesDir(null)}${File.separator}IMG_TMP")
            if (!tmpDir.exists() && !tmpDir.isDirectory) {
                tmpDir.mkdirs()
                Log.d("A_MediaUtil", "Create new directory $tmpDir")
            }
            return File(context.getExternalFilesDir(null), "IMG_TMP/IMG_${sdf.format(Date())}.$extension")
        }

        fun createFile(context: Context, extension: String, prefix: String): File {
            val sdf = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS", Locale.US)
            return File(context.getExternalFilesDir(null), "VID_$prefix${sdf.format(Date())}.$extension")
        }

        fun createPNG(context: Context, extension: String): File {
            val sdf = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS", Locale.US)
            return File(context.getExternalFilesDir(null), "PNG_${sdf.format(Date())}$extension")
        }

        fun writeIntoStorage(file: File, bitmap: Bitmap) {
            try {
                FileOutputStream(file.absolutePath).use {
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, it)
                }
                Log.d("ToolKitFragment", "textSticker saved")
            } catch (e: IOException) {
                Log.d("ToolKitFragment", "textSticker isn't saved")
            }
        }

        fun removeFile(context: Context, file: File) =
                context.getExternalFilesDir(file.absolutePath)?.delete()

        fun clearPngFiles(context: Context) {
            try {
                val tmpDir = File("${context.getExternalFilesDir(null)}${File.separator}IMG_TMP")
                deleteTmpFiles(tmpDir)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }

        private fun deleteTmpFiles(dir: File?): Boolean {
            return if (dir != null && dir.isDirectory) {
                val children = dir.list()
                for (i in children.indices) {
                    val success = deleteTmpFiles(File(dir, children[i]))
                    if (!success) {
                        return false
                    }
                    dir.delete()
                }
                return true
            } else if (dir != null && dir.isFile) {
                dir.delete()
            } else {
                false
            }
        }

        private fun getAllMedia(context: Context): List<VideoData> {

            val videoList = mutableListOf<VideoData>()

            val projection = arrayOf(
                    MediaStore.Video.Media._ID,
                    MediaStore.Video.Media.DISPLAY_NAME,
                    MediaStore.Video.Media.DURATION,
                    MediaStore.Images.ImageColumns._ID
            )

            val cursor: Cursor? = context.contentResolver
                    .query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, projection, null, null, MediaStore.Video.VideoColumns.DATE_MODIFIED + " DESC")
            try {
                cursor!!.moveToFirst()
                val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID)
                do {

                    val id = cursor.getLong(idColumn)
                    val contentUri: Uri = ContentUris.withAppendedId(
                            MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                            id
                    )
                    val timeInSec = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION)) / 1000.0
                    val length = File(contentUri.toString()).length()
                    val thumbUrl = ""
                    videoList.add(
                            VideoData(
                                    timeInSec,
                                    listOf(),
                                    0,
                                    true,
                                    thumbUrl,
                                    length,
                                    contentUri.toString(),
                                    false,
                                    "",
                                    false,
                                    id
                            )
                    )

                } while (cursor.moveToNext())
                cursor.close()
            } catch (e: Exception) {
                e.printStackTrace()
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            return videoList
        }

        suspend fun getAllMediaAsync(
                context: Context
        ): List<VideoData> = suspendCancellableCoroutine { cont ->
            val result = getAllMedia(context)
            cont.resume(result, onCancellation = {})
        }

        suspend fun getLatestMediaAsync(
                context: Context
        ): String? = suspendCancellableCoroutine { cont ->
            val result = getLatestMedia(context)
            cont.resume(result, onCancellation = {})
        }

        private fun getLatestMedia(context: Context): String? {
            var path: String? = null
            val projection = arrayOf(
                    MediaStore.Video.Media._ID
            )
            val cursor: Cursor? = context.contentResolver
                    .query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, projection,
                            null, null,
                            MediaStore.Video.VideoColumns.DATE_MODIFIED + " DESC")
            try {
                cursor!!.moveToFirst()
                val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID)
                val id = cursor.getLong(idColumn)
                val contentUri: Uri = ContentUris.withAppendedId(
                        MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                        id
                )
                path = contentUri.toString()

                cursor.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return path
        }

        fun createThumbFile(context: Context, file: File, thumbFile: File) {
            Glide.with(context)
                    .asBitmap()
                    .load(file.absolutePath)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(resource: Bitmap, transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?) {
                            try {
                                FileOutputStream(thumbFile.absolutePath).use { out ->
                                    resource.compress(Bitmap.CompressFormat.JPEG, 100, out)
                                }
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {}
                    })
        }

        fun createBitmapFile(context: Context, bitmap: Bitmap): File {
            val bitmapFile = createPngFile(context, EXTENSION_PNG)
            val byteArrayOutputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream)
            val bitmapData = byteArrayOutputStream.toByteArray()
            with(FileOutputStream(bitmapFile)) {
                write(bitmapData)
                flush()
                close()
            }
            return bitmapFile
        }

        fun convertSecsToTimeString(timeSeconds: Int): String? {
            //Convert number of seconds into hours:mins:seconds string
            val hours = timeSeconds / 3600
            val mins = timeSeconds % 3600 / 60
            val secs = timeSeconds % 60
            return String.format("%02d:%02d:%02d", hours, mins, secs)
        }
    }




}