package com.ellorem.oxygean.video

//import com.arthenica.mobileffmpeg.Config.RETURN_CODE_CANCEL
//import com.arthenica.mobileffmpeg.Config.RETURN_CODE_SUCCESS
import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.core.view.children
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arthenica.mobileffmpeg.Config
import com.arthenica.mobileffmpeg.FFmpeg
import com.arthenica.mobileffmpeg.FFmpeg.RETURN_CODE_CANCEL
import com.arthenica.mobileffmpeg.FFmpeg.RETURN_CODE_SUCCESS
import com.ellorem.oxygean.video.toolkits.SelectedTool
import com.ellorem.oxygean.video.toolkits.ToolKitFragment
import com.ellorem.oxygean.video.util.ClipsData
import com.ellorem.oxygean.video.util.GetVideoRotationInDegrees
import com.ellorem.oxygean.video.util.MediaUtil
import com.ellorem.oxygean.video.util.MediaUtil.Companion.convertSecsToTimeString
import com.ellorem.oxygean.video.videodata.EditVideoData
import com.ellorem.oxygean.video.videodata.FFMPEG_TAG
import com.ellorem.oxygean.video.videodata.makeCopyOf
import com.google.gson.Gson
import kotlinx.android.synthetic.main.block_cutting_splitting.*
import kotlinx.android.synthetic.main.fragment_edit.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import java.io.File
import java.util.*
import kotlin.concurrent.fixedRateTimer
import kotlin.math.roundToInt


class EditFragment : ToolKitFragment(), CameraActivity.KeyListener {

    private var touchHelper: ItemTouchHelper? = null
    private var isSplitting: Boolean = false

    private var percentLeft: Double = 0.0
    private var rawLeftX: Int = 0
    private var rawRightX: Int = 0
    private var percentRight: Double = 100.0

    //cuts thumbnails recycler
    private var thumbnailsListRecyclerView: RecyclerView? = null

    //steps recycler
    private var itemMoveCallbackListener: ItemMoveCallbackListener? = null
    private var listOfVideosAdapter: ThumbnailAdapter? = null
    private var listOfVideosView: RecyclerView? = null

    private var currentVideoPlayingPosition = 0

    //Should I change this value each time???
    private var isEditing = false

//    private val disposable = CompositeDisposable()

    private var cutSelected: Boolean = true

    private var listOfVideosViewEventListener = object : ThumbnailAdapter.OnStartDragListener {

        override fun onStartDrag(viewHolder: RecyclerView.ViewHolder) {
            if (isEditing) {
                return
            }
            touchHelper?.startDrag(viewHolder)
        }

        override fun onClickItem(position: Int) {
            if (position == -1 || position >= resultVideos.size) {
                return
            }
            if (isEditing) {
                return
            }

            pauseCurrentVideoAndTimer()
            playNextVideo(position)
            when (selectedTool) {
                SelectedTool.FiltersToolSelected -> initFilterTool()
                SelectedTool.TextEditorSelected -> {
                    fontDescriptor = null
                }
                else -> {
                }
            }
        }

        override fun onDataRemoved(pos: Int) {
            if (pos == -1 || pos >= resultVideos.size) {
                return
            }
            pauseCurrentVideoAndTimer()
            if (pos == currentVideoPlayingPosition - 1) {
                if (currentVideoPlayingPosition == resultVideos.size - 1) {
                    currentVideoPlayingPosition--
                }
            }
            resultVideos.removeAt(pos)
            playNextVideo()
        }

        override fun onDataReordered(from: Int, to: Int) {
            Collections.swap(resultVideos, from, to)
        }
    }

    fun pauseCurrentVideoAndTimer() {
        stopPlayVideo()
    }

    private var viewGroup:ViewGroup? = null
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        viewGroup = container
        return inflater.inflate(R.layout.fragment_edit, container, false)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initAllView()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initAllView() {
        initScreenMetrics()
        initNavigationButton()
        initResultVideosList()
        initVideoPlayer()

        thumbnailsListRecyclerView = requireView().findViewById(R.id.thumbnailsList)

        initApplyButtonLogic()

        listOfVideosView = list_of_videos_copy

        val layoutManager = LinearLayoutManager(requireContext())
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        listOfVideosView!!.layoutManager = layoutManager

        listOfVideosAdapter = ThumbnailAdapter(requireContext(), resultVideos, this, lifecycleScope)
        listOfVideosAdapter?.selectedVideo?.observe(viewLifecycleOwner, Observer{
            if(!isSplitting)
                listOfVideosAdapter!!.updateVideos(resultVideos)
        })
        listOfVideosAdapter!!.addListener(listOfVideosViewEventListener)
        itemMoveCallbackListener = ItemMoveCallbackListener(listOfVideosAdapter!!)
        listOfVideosView!!.adapter = listOfVideosAdapter

        touchHelper = ItemTouchHelper(itemMoveCallbackListener!!)
        touchHelper!!.attachToRecyclerView(listOfVideosView)

        split_button.background = null
        cutSelected = true

        cut_button.setOnClickListener {
            cutSelected = true
//            resultVideos.forEach { video -> video.isTemporaryForSplit = false }
            resultVideos.removeAll(resultVideos.filter { video -> video.isTemporaryForSplit });

            resetSliderToPlaying()

            listOfVideosAdapter!!.updateVideos(resultVideos)
            listOfVideosAdapter!!.removeHighLight()

            cut_button.setBackgroundResource(R.drawable.border)
            split_button.background = null
        }

        split_button.setOnClickListener {
            cutSelected = false
            resetSliderToPlaying()

            resultVideos.removeAll(resultVideos.filter { video -> video.isOriginalForSplit })

            split_button.setBackgroundResource(R.drawable.border)
            cut_button.background = null
        }

        cancel_edit.setOnClickListener {
            if (!cutSelected) {
                resultVideos.map { editedVideo ->
                    if (editedVideo.isOriginalForSplit) {
                        editedVideo.isSelected = true
                    }
                }
            }
            resultVideos.removeAll(resultVideos.filter { video -> video.isTemporaryForSplit })
            resultVideos = resultVideos.onEach { it.isOriginalForSplit = false }


            resetSliderToPlaying()
            split_button.isEnabled = true
            cut_button.isEnabled = true

            if (!cutSelected) {
                listOfVideosAdapter!!.updateVideos(resultVideos, true)
                listOfVideosAdapter!!.removeHighLight()
            }

            playNextVideo()
        }

        finish_edit.setOnClickListener {
            isSplitting = false
            if (cutSelected) {
                cutCurrentVideo()
            } else {
                resultVideos.removeAll(resultVideos.filter { video ->
                    video.isOriginalForSplit })
                resultVideos = resultVideos.map { video ->
                    makeCopyOf(video).apply {
                        isTemporaryForSplit = false
                    }
                }.toMutableList()
            }

            resetSliderToPlaying()
            split_button.isEnabled = true
            cut_button.isEnabled = true

            listOfVideosAdapter!!.removeHighLight()
            playNextVideo()
        }

        requireView().findViewById<View>(R.id.seek_bar).setOnTouchListener { _, _ -> true }

        val overlayLeft = requireView().findViewById<View>(R.id.edit_overlay_left)
        requireView().findViewById<View>(R.id.button_slide_left).setOnTouchListener(View.OnTouchListener { view, event ->
            val params = RelativeLayout.LayoutParams(rawLeftX, overlayLeft.height)
            params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE)
            overlayLeft.layoutParams = params
            overlayLeft.requestLayout()
            return@OnTouchListener startEdit(view, event, true)
        })

        val overlayRight = requireView().findViewById<View>(R.id.edit_overlay_right)
        requireView().findViewById<View>(R.id.button_slide_right).setOnTouchListener(View.OnTouchListener { view, event ->
            val params = RelativeLayout.LayoutParams(screenWidth - rawRightX, overlayRight.height)
            params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE)
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE)
            overlayRight.layoutParams = params
            overlayRight.requestLayout()
            return@OnTouchListener startEdit(view, event, false)
        })

        initLeftToolBar()
        initListOfFilters()
    }


    private fun initResultVideosList() {
        val videos = (requireActivity() as CameraActivity).videos

        for (video in videos) {
            resultVideos.add(EditVideoData(video))
        }
    }

    private fun initApplyButtonLogic() {
        requireView().findViewById<View>(R.id.next_layout).setOnClickListener {

            loading_layout.visibility = View.VISIBLE
            //why???
            Thread.sleep(100)
            pauseCurrentVideoAndTimer()
            reduceVideoSizeAndFinish()
        }
    }

    private fun initNavigationButton() {
        navigation.setOnClickListener {
            showMessageOnBackPressed()
        }
    }

    private fun initScreenMetrics() {
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        screenWidth = displayMetrics.widthPixels
    }

    private fun resetSliderToPlaying() {
        requireView().findViewById<View>(R.id.next_layout).visibility = View.VISIBLE
        edit_control_layout.visibility = View.GONE
        requireView().findViewById<View>(R.id.line_above_slider).setBackgroundColor(Color.WHITE)
        requireView().findViewById<View>(R.id.line_below_slider).setBackgroundColor(Color.WHITE)
        requireView().findViewById<ImageView>(R.id.button_slide_right).setImageResource(R.mipmap.bgslide)
        requireView().findViewById<ImageView>(R.id.button_slide_left).setImageResource(R.mipmap.bgslide)
        requireView().findViewById<ImageView>(R.id.button_slide_right).setBackgroundColor(Color.BLACK)
        requireView().findViewById<ImageView>(R.id.button_slide_left).setBackgroundColor(Color.BLACK)

        var params = RelativeLayout.LayoutParams(0, requireView().findViewById<View>(R.id.edit_overlay_right).height)
        requireView().findViewById<View>(R.id.edit_overlay_right).layoutParams = params

        params = RelativeLayout.LayoutParams(0, requireView().findViewById<View>(R.id.edit_overlay_left).height)
        requireView().findViewById<View>(R.id.edit_overlay_left).layoutParams = params

        val margin = requireContext().resources.getDimension(R.dimen.margin_large)
        requireView().findViewById<View>(R.id.button_slide_left).x = margin
        requireView().findViewById<View>(R.id.button_slide_right).x = screenWidth - requireView().findViewById<View>(R.id.button_slide_right).width - margin

        rawRightX = screenWidth - requireView().findViewById<View>(R.id.button_slide_right).width
        rawLeftX = 0
        percentRight = 100.0
        percentLeft = 0.0

        if (listOfVideosAdapter != null) {
            listOfVideosAdapter!!.addListener(listOfVideosViewEventListener)
            if (touchHelper == null) {
                touchHelper = ItemTouchHelper(itemMoveCallbackListener!!)
            }
            if (touchHelper != null) {
                touchHelper!!.attachToRecyclerView(listOfVideosView)
            }
        }
        isEditing = false
    }

    var dX: Float? = null
    fun View.setAllEnabled(enabled: Boolean) {
        isEnabled = enabled
        if (this is ViewGroup) children.forEach { child -> child.setAllEnabled(enabled) }
    }

    private fun moveViewHorizontally(view: View, event: MotionEvent, isLeft: Boolean): Boolean {
        when (event.getAction()) {
            MotionEvent.ACTION_DOWN -> {
                //Log.d("Aaaa", "motinodown ")
                dX = view.x - event.getRawX()
            }

            MotionEvent.ACTION_UP -> {
//                Log.d("Aaaa", "motion up, rawRightX "+rawRightX + " rawLeft "+rawLeftX +" isLeft " +isLeft)
                var accuratePercent = view.x / screenWidth * 100
                Log.d("EditFragment", "left " + percentLeft + "right " + percentRight + " " + accuratePercent)
                if (accuratePercent <= 5)
                    percentLeft = 0.0
                if (percentRight >= 95)
                    percentRight = 100.0
                if (!cutSelected) {
                    loading.visibility = View.VISIBLE
                    viewGroup?.setAllEnabled(false)
                    lifecycleScope.launch(Dispatchers.Default) {
                        splitVideoAsync()
                    }
                }
            }

            MotionEvent.ACTION_MOVE -> {
                val prevX = (event.getRawX() + dX!!).roundToInt()
                //Log.d("Aaaa", "ACTION_MOVE "+(rawLeftX -prevX))
                //sliding back, not to goo beyound begging of screen
                if (isLeft && ((prevX - rawLeftX) < 0) && rawLeftX < requireView().findViewById<View>(R.id.button_slide_left).width + 10) {
                    return true
                }
                if (!isLeft &&
                        ((rawLeftX - prevX) > 0) && rawRightX > 0 && rawLeftX + requireView().findViewById<View>(R.id.button_slide_left).width * 2F > rawRightX) {
                    return true
                }

                if (isLeft &&
                        ((rawLeftX - prevX) < 0) && rawLeftX + requireView().findViewById<View>(R.id.button_slide_left).width * 2F > rawRightX) {
                    return true
                }

                val percent = 100.0 * event.getRawX() / (screenWidth * 1.0f)
                if (isLeft) {
                    rawLeftX = (event.getRawX() + dX!!).roundToInt()
                    percentLeft = percent
                } else {
                    rawRightX = (event.getRawX() + dX!!).roundToInt()
                    percentRight = percent
                }
                view.animate()
                        .x(event.getRawX() + dX!!)
                        .setDuration(0)
                        .start()
            }
            else -> return false
        }
        return true
    }

    private fun cutCurrentVideo() {
        val duration = currentVideo!!.video.length
        val startTime = duration * percentLeft / 100.0
        val endTime = duration * percentRight / 100.0
        val video = createEditedVideo(startTime, endTime, "cut", true);
        video?.let {
            makeCopyOf(currentVideo!!).apply {
                this.video = it
                resultVideos[resultVideos.indexOf(currentVideo!!)] = this
                requireActivity().runOnUiThread {
                    setCurrentVideoItem(this)
                }
            }
            listOfVideosAdapter!!.updateVideos(resultVideos)
        }
    }

    private suspend fun splitVideoAsync():Boolean? = suspendCancellableCoroutine { cont ->
        val result = splitCurrentVideoToTempFiles()
        cont.resume(result, onCancellation = {})
    }

    private fun splitCurrentVideoToTempFiles():Boolean? {
        resultVideos.removeAll(resultVideos.filter { video -> video.isTemporaryForSplit });
        val originalForSplit = resultVideos.find { editedVideo -> editedVideo.isOriginalForSplit }
        if (originalForSplit != null) {
            requireActivity().runOnUiThread {
                setCurrentVideoItem(originalForSplit)
            }
        }

        val duration = currentVideo!!.video.length
        val startTime = duration * percentLeft / 100.0
        val endTime = duration * percentRight / 100.0

        val firstSplitVideo = createEditedVideo(0.0, startTime, "split1_", false)
        val secondVideoStartTime = firstSplitVideo?.length ?: startTime
        val secondSplitVideo = createEditedVideo(secondVideoStartTime, endTime, "split2_", false)
        var thirdSplitVideo: MediaUtil.VideoData? = null
        if (percentRight != 100.0) {
            val thirdVideoStartTime = if (secondSplitVideo != null) (secondVideoStartTime + secondSplitVideo!!.length) else endTime
            thirdSplitVideo = createEditedVideo(thirdVideoStartTime, duration * 1.0, "split3_", false)
        }
        var indexAfterCurrent = resultVideos.indexOf(currentVideo!!) + 1
        val initialCurrentIndex = indexAfterCurrent
        //use makeCopyOf to save all editing properties
//        if (!currentVideo!!.isTemporaryForSplit) {
//            resultVideos.set(resultVideos.indexOf(currentVideo!!),
//                    makeCopyOf(currentVideo!!).apply {
//                        isOriginalForSplit = true
//                    })
//        }
        if (!currentVideo!!.isTemporaryForSplit) {
            currentVideo!!.isOriginalForSplit = true
            resultVideos[resultVideos.indexOf(currentVideo!!)] = currentVideo!!
        }

        if (firstSplitVideo != null) {
            resultVideos.add(indexAfterCurrent, makeCopyOf(currentVideo!!).apply {
                video = firstSplitVideo
                isTemporaryForSplit = true
            }
            )
            indexAfterCurrent++
        }
        if (secondSplitVideo != null) {
            resultVideos.add(indexAfterCurrent, makeCopyOf(currentVideo!!).apply {
                video = secondSplitVideo
                isTemporaryForSplit = true
            }
            )
            indexAfterCurrent++
        }
        if (thirdSplitVideo != null) {
            resultVideos.add(indexAfterCurrent, makeCopyOf(currentVideo!!).apply {
                video = thirdSplitVideo
                isTemporaryForSplit = true
            }
            )
        }

        requireActivity().runOnUiThread {
            setCurrentVideoItem(resultVideos[initialCurrentIndex % resultVideos.size])
            val videosForAdapter = mutableListOf<EditVideoData>()
            for (editedVideo in resultVideos) {
                if (!editedVideo.isOriginalForSplit) {
                    videosForAdapter.add(editedVideo)
                }
            }
            loading.visibility = View.GONE
            viewGroup?.setAllEnabled(true)
            listOfVideosAdapter!!.updateVideos(videosForAdapter, true)
        }
        return true
    }

    private fun createEditedVideo(startTime: Double, endTime: Double, filePrefix: String, shouldCorrectEndTime: Boolean): MediaUtil.VideoData? {
        if (endTime - startTime == 0.0) {
            return null
        }
        var startTimeNew = startTime;
        var endTimeNew = endTime;
        if (endTime - startTime < 1.0) {
            if (startTime >= 1.0) {
                startTimeNew -= 1.0
            } else {
                if (currentVideo!!.video.length > 1 + endTimeNew) {
                    endTimeNew += 1.0
                } else {
                    endTimeNew = currentVideo!!.video.length
                }
            }
        }
        if(endTimeNew-startTimeNew<=1)
            return null

        val editedFile = MediaUtil.createFile(requireContext(), "mp4", filePrefix)
        val startTime1 = convertSecsToTimeString(startTimeNew.toInt())
        val endTime1 = convertSecsToTimeString(endTimeNew.toInt())

//        val complexCommand = arrayOf("-ss", "" + startMs / 1000, "-y", "-i", inputFileAbsolutePath, "-t", "" + (endMs - startMs) / 1000, "-s", "320x240", "-r", "15", "-vcodec", "mpeg4", "-b:v", "2097152", "-b:a", "48000", "-ac", "2", "-ar", "22050", outputFileAbsolutePath)
//        val command = "-ss "+startTime1 + " -i " + currentVideo?.video?.videoURL + " -to "+endTime1+ " -c copy "+editedFile.absolutePath
//        val command = "-y -noaccurate_seek -ss "+startTime1 + " -i " + currentVideo?.video?.videoURL + " -vcodec copy -acodec copy -to "+endTime1+ " -sn "+editedFile.absolutePath
//        val command =  "-i " + currentVideo?.video?.videoURL + " -c copy -copyinkf -ss "+startTime1 + " -to "+endTime1+ " "+editedFile.absolutePath
        val length = convertSecsToTimeString(endTimeNew.toInt()-startTimeNew.toInt())
        val command = "-ss "+startTime1 + " -y -i " + currentVideo?.video?.videoURL + " -t "+ length + " -r 15 -vcodec mpeg4 -b:v 2097152 -b:a 48000 -ac 2 -ar 22050 "+editedFile.absolutePath
        val rc = FFmpeg.execute(command)
        if (rc == RETURN_CODE_SUCCESS) {
            Log.i(Config.TAG, "Command execution completed successfully.")
            val thumbFile = MediaUtil.createFile(requireContext(), "jpeg")
            MediaUtil.createThumbFile(requireContext(), editedFile, thumbFile)
            return MediaUtil.VideoData(
                    endTimeNew - startTime,
                    listOf(),
                    0,
                    false,
                    thumbFile.absolutePath,
                    editedFile.length(),
                    editedFile.absolutePath,
                    currentVideo!!.video.shouldReduceSize)
        } else if (rc == RETURN_CODE_CANCEL) {
            Log.i(Config.TAG, "Command execution cancelled by user.")
        } else {
            Log.i(Config.TAG, String.format("Command execution failed with rc=%d and the output below.", rc))
        }
        return null
//
//        val editedFile = MediaUtil.createFile(requireContext(), "mp4", filePrefix)
//        val durationFinal = VideoParser().startTrim(File(currentVideo!!.video.videoURL), editedFile, startTimeNew, endTimeNew, currentVideo!!.video.length, shouldCorrectEndTime)
//        val thumbFile = MediaUtil.createFile(requireContext(), "jpeg")
//        MediaUtil.createThumbFile(requireContext(), editedFile, thumbFile)
//        return MediaUtil.VideoData(
//                durationFinal,
//                listOf(),
//                0,
//                false,
//                thumbFile.absolutePath,
//                editedFile.length(),
//                editedFile.absolutePath,
//                currentVideo!!.video.shouldReduceSize)
//        return null
    }

    private fun startEdit(view: View, event: MotionEvent, isLeft: Boolean): Boolean {
        isSplitting = true
        pauseCurrentVideoAndTimer()

        requireView().findViewById<View>(R.id.next_layout).visibility = View.GONE
        edit_control_layout.visibility = View.VISIBLE
        requireView().findViewById<View>(R.id.line_above_slider).setBackgroundColor(requireContext().getColor(R.color.color_pink))
        requireView().findViewById<View>(R.id.line_below_slider).setBackgroundColor(requireContext().getColor(R.color.color_pink))
        requireView().findViewById<ImageView>(R.id.button_slide_right).setImageResource(R.mipmap.bgslidepink)
        requireView().findViewById<ImageView>(R.id.button_slide_left).setImageResource(R.mipmap.bgslidepink)
        requireView().findViewById<ImageView>(R.id.button_slide_right).setBackgroundColor(Color.WHITE)
        requireView().findViewById<ImageView>(R.id.button_slide_left).setBackgroundColor(Color.WHITE)
        if (!cutSelected) {
            requireView().findViewById<View>(R.id.edit_overlay_left).setBackgroundColor(requireContext().getColor(R.color.color_semitransparent))
            requireView().findViewById<View>(R.id.edit_overlay_right).setBackgroundColor(requireContext().getColor(R.color.color_semitransparent))
        } else {
            requireView().findViewById<View>(R.id.edit_overlay_left).setBackgroundColor(requireContext().getColor(R.color.overlay_pink))
            requireView().findViewById<View>(R.id.edit_overlay_right).setBackgroundColor(requireContext().getColor(R.color.overlay_pink))
        }
        requireView().findViewById<View>(R.id.split_button).isEnabled = false
        requireView().findViewById<View>(R.id.cut_button).isEnabled = false

        if (listOfVideosAdapter != null) {
            listOfVideosAdapter!!.removeListener(listOfVideosViewEventListener)
            if (touchHelper != null) {
                touchHelper!!.attachToRecyclerView(null)
            }
        }
        isEditing = true

        return moveViewHorizontally(view, event, isLeft)
    }

    private fun playFirstVideo() {
        currentVideoPlayingPosition = 0
        playNextVideo()
    }

    override fun playNextVideo(position: Int) {
        if (position >= 0) currentVideoPlayingPosition = position

        if (currentVideoPlayingPosition >= resultVideos.size) {
            stopPlayVideo()
            currentVideoPlayingPosition = 0
        }
        requireActivity().runOnUiThread {
            setCurrentVideoItem(resultVideos[currentVideoPlayingPosition])
        }
        currentVideo?.let {
            showThumbnailsForVideo(it)
        }
        startPlayVideo()
        startTimer()
        if (selectedTool == SelectedTool.TextEditorSelected) turnOnTextManager()

        if (position == PLAY_NEXT || position >= 0) {
            currentVideoPlayingPosition++
        }
    }

    override fun updateListOfVideos() {
        listOfVideosAdapter?.updateVideos(resultVideos, true)
    }

    override fun updateListOfVideosAdapter() {
        currentVideo?.let {
            listOfVideosAdapter?.setSelectedVideo(it)
        }
    }

    private fun startTimer() {
        timer?.cancel()

        val duration = currentVideo!!.video.length
        var step = (duration * 10).toLong()
        var progress = 0
        if (step < 1) {
            step = 1
        }

        timer = fixedRateTimer(period = step) {
            val seekBar = requireView().findViewById<SeekBar>(R.id.seek_bar)
            try {
                if (requireView().isAttachedToWindow) {
                    seekBar.progress = progress++
                }
            } catch (e: Throwable) {
                Log.e("EditFragment error", e.message ?: "")
            }
        }
    }

    private fun showThumbnailsForVideo(video: EditVideoData) {
        if (thumbnailsListRecyclerView?.adapter == null
                || (thumbnailsListRecyclerView?.adapter as? ThumbnailsVideoPreviewAdapter)?.videoForPreview != video
                || (thumbnailsListRecyclerView?.adapter as? ThumbnailsVideoPreviewAdapter)?.filter != video.videoFilterAccepted.appliedFilter) {
            val layoutManager = LinearLayoutManager(requireContext()).apply {
                orientation = LinearLayoutManager.HORIZONTAL
            }
            thumbnailsListRecyclerView!!.layoutManager = layoutManager
            if (resultVideos.size > currentVideoPlayingPosition) {
                thumbnailsListRecyclerView!!.adapter = ThumbnailsVideoPreviewAdapter(video, this, lifecycleScope)
            }
        }
    }

    override fun onPause() {
        pauseCurrentVideoAndTimer()
        super.onPause()
    }

    override fun onStart() {
        super.onStart()
        (requireActivity() as CameraActivity).addListener(this)
    }

    override fun onStop() {
        (requireActivity() as CameraActivity).removeListener(this)
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        loading_layout.visibility = View.GONE
        playFirstVideo()
    }

    override fun onDestroy() {
//        disposable.clear()
        super.onDestroy()
    }

    private fun reduceVideoSizeAndFinish() {
        requireView().findViewById<View>(R.id.loading_layout).visibility = View.VISIBLE
        requireView().findViewById<View>(R.id.next_layout).isEnabled = false

        lifecycleScope.launch(Dispatchers.Default) {
            applyFilterForVideo()
            reduceAndLeaveAsync()
        }
    }

    private suspend fun reduceAndLeaveAsync(
    ): String? = suspendCancellableCoroutine { cont ->
        val result = reduceAndLeaveSync()
        cont.resume(result, onCancellation = {})
    }

    private fun reduceAndLeaveSync(): String? {
        requireActivity().runOnUiThread {
            requireView().findViewById<View>(R.id.loading_layout).setVisibility(View.VISIBLE)
        }

        val videosToSend = mutableListOf<MediaUtil.VideoData>()
        var i = 0

        for (resultVideo in resultVideos.filter { v -> !v.isTemporaryForSplit }) {
            val video = resultVideo.video
            if (video.shouldReduceSize) {
                val oldPath = video.videoURL
                val tmpFile = MediaUtil.createFile(requireContext(), "mp4", "tmp_")
                File(video.videoURL).copyTo(tmpFile)
                val editedFile = MediaUtil.createFile(requireContext(), "mp4", "REDUCED_")
                //current
//                val commandComplete = arrayOf("ffmpeg", "-i", video.videoURL, "-vf", "scale=530:-2,setsar=1:1", "-vcodec", "libx264", "-b:v", "768k", "-r", "20", editedFile.absolutePath)
                //proposal 1
                val commandComplete = arrayOf("ffmpeg", "-i", video.videoURL, "-vf", "scale=iw/3:ih/3", "-vcodec", "libx264", "-crf", "20", editedFile.absolutePath)
                // proposal 2
//                val commandComplete = arrayOf("ffmpeg", "-i", video.videoURL, "-vf", "scale=iw/2:ih/2", "-vcodec", "libx264", "-crf", "20", editedFile.absolutePath)

                Log.d("EditFragment", "reducing " + video.videoURL)
                try {
//                    val command = "-i " + video.videoURL + " -vf scale=iw/2:ih/2 -vcodec libx264 -crf 18 " + editedFile.absolutePath
//                    val workFolder: String = requireContext().getExternalFilesDir(null)!!.absolutePath
//                    //vk.run(GeneralUtils.utilConvertToComplex(commandComplete), workFolder, requireContext());
//                    vk.run(commandComplete, workFolder, requireContext());

//                    val command = "-i " + video.videoURL + " -vf scale=iw/3:ih/3 " + editedFile.absolutePath
                    Log.d(FFMPEG_TAG, "start execute compression")
//                    val command = "-i " + video.videoURL + " -vf scale=iw/2:ih/2 -vcodec libx264 -crf 18 " + editedFile.absolutePath
                    val command = "-i " + video.videoURL + " -vf pad=width=ceil(iw/2)*2:height=ceil(ih/2)*2 -vcodec libx264 -crf 18 " + editedFile.absolutePath
                    val rc = FFmpeg.execute(command)
                    if (rc == RETURN_CODE_SUCCESS) {
                        Log.i(Config.TAG, "Command execution completed successfully.")
                    } else if (rc == RETURN_CODE_CANCEL) {
                        Log.i(Config.TAG, "Command execution cancelled by user.")
                    } else {
                        Log.i(Config.TAG, String.format("Command execution failed with rc=%d and the output below.", rc))
                    }
                    Log.d(FFMPEG_TAG, "finish execute compression")
                    tmpFile.delete()
                    if (oldPath.indexOf(requireContext().getExternalFilesDir(null).toString()) != -1) {
                        File(oldPath).delete()
                    }
                    Log.d("EditFragment", "ffmpeg4android finished successfully")
                    video.videoURL = editedFile.absolutePath
                    video.fileSize = editedFile.length()
                    video.shouldReduceSize = false

                } catch (e: Throwable) {
                    Log.e("EditFragment", "vk run exception.", e)
                }
            }

            video.order = i
            val videoNotOriginal = video.copy(isOriginal = false)
            val rotation = GetVideoRotationInDegrees.getRotation(video.videoURL)
            videoNotOriginal.rotation = rotation.toString()
            videosToSend.add(videoNotOriginal)
            i++
        }

        val originals = (requireActivity() as CameraActivity).videos
        for (video in originals) {
            val videoOriginal = video.copy(isOriginal = true)
            videosToSend.add(videoOriginal)
        }
        val clipsData = ClipsData(videosToSend, mutableListOf())
        val clipsJson = Gson().toJson(clipsData)

        //Log.d("EditFragment" , "result json:" + clipsJson)
        (requireActivity() as CameraActivity).returnResult(clipsJson);

        return ""
    }

    private fun showMessageOnBackPressed() {
        var changed = false
        for (resultVideo in resultVideos) {
            val video = resultVideo.video
            if (!video.isOriginal) {
                changed = true
            }
        }
        if (changed) {

            showMessage(requireContext().getString(R.string.return_from_edit), object : OnClickListenerInterface {
                override fun onClickYes() {
                    pauseCurrentVideoAndTimer()
                    (requireActivity() as CameraActivity).removeCurrentFragment()
                }

                override fun onClickNo() {
                    hideMessage()
                }
            })
        } else {
            pauseCurrentVideoAndTimer()
            (requireActivity() as CameraActivity).removeCurrentFragment()
        }
    }

    private fun showMessage(description: String, onClickListenerInterface: OnClickListenerInterface) {
        requireView().findViewById<RelativeLayout>(R.id.message).visibility = View.VISIBLE
        requireView().findViewById<TextView>(R.id.message_desc).text = description
        requireView().findViewById<View>(R.id.button_cancel).setOnClickListener { onClickListenerInterface.onClickNo() }
        requireView().findViewById<View>(R.id.button_yes).setOnClickListener { onClickListenerInterface.onClickYes() }
    }

    private fun hideMessage() {
        requireView().findViewById<RelativeLayout>(R.id.message).visibility = View.GONE
    }

    interface OnClickListenerInterface {
        fun onClickYes()
        fun onClickNo()
    }

    override fun onBackPressed() {
        showMessageOnBackPressed()
    }
}