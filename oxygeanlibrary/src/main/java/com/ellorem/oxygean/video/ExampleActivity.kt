package com.ellorem.oxygean.video

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class ExampleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_example)

       startActivityForResult(Intent(this, PermissionsActivity::class.java), 104)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            findViewById<TextView>(R.id.result_text).text = data!!.getStringExtra(CameraActivity.DATA_OUTPUT)
        }
    }
}