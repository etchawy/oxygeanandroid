package com.ellorem.oxygean.video.util

import android.content.Context
import android.util.Log
import com.arthenica.mobileffmpeg.Config
import com.arthenica.mobileffmpeg.FFmpeg
import com.arthenica.mobileffmpeg.FFmpeg.RETURN_CODE_CANCEL
import com.arthenica.mobileffmpeg.FFmpeg.RETURN_CODE_SUCCESS
import com.coremedia.iso.boxes.Container
import com.googlecode.mp4parser.FileDataSourceImpl
import com.googlecode.mp4parser.authoring.Movie
import com.googlecode.mp4parser.authoring.Track
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator
import com.googlecode.mp4parser.authoring.tracks.AppendTrack
import com.googlecode.mp4parser.authoring.tracks.CroppedTrack
import kotlinx.coroutines.suspendCancellableCoroutine
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Exception
import java.nio.channels.WritableByteChannel
import java.util.*


class VideoParser {

    @Throws(IOException::class)
    fun startTrim(src: File, dst: File, startSec: Double, endSec: Double, duration: Double, shouldCorrectEndTime: Boolean) : Double {
        val file = FileDataSourceImpl(src)
        try {
            val movie: Movie = MovieCreator.build(file)
            // remove all tracks we will create new tracks from the old
            val tracks: List<Track> = movie.getTracks()
            movie.setTracks(LinkedList<Track>())
            var startTime = startSec// / 1000.toDouble()
            var endTime = endSec// / 1000.toDouble()
            var timeCorrected = false
            // Here we try to find a track that has sync samples. Since we can only start decoding
            // at such a sample we SHOULD make sure that the start of the new fragment is exactly
            // such a frame
            for (track in tracks) {
                if (track.getSyncSamples() != null && track.getSyncSamples().size > 0) {
                    if (timeCorrected) {
                        // This exception here could be a false positive in case we have multiple tracks
                        // with sync samples at exactly the same positions. E.g. a single movie containing
                        // multiple qualities of the same video (Microsoft Smooth Streaming file)
                        Log.e("VideoParser", "The startTime has already been corrected by another track with SyncSample. Not Supported.")
                    }
                    if (startSec > 0) {
                        startTime = correctTimeToSyncSample(track, startSec, false)
                    }
                    if (shouldCorrectEndTime) {
                        if (endTime < duration) {
                            endTime = correctTimeToSyncSample(track, endSec, true)
                        }
                    }
                    //startTime = startMs
                    //endTime = endMs
                    timeCorrected = true
                }
            }
            if (startTime == endTime) {
                endTime = endSec
            }

            for (track in tracks) {
                var currentSample: Long = 0
                var currentTime = 0.0
                var startSample: Long = -1
                var endSample: Long = -1
                for (i in 0 until track.getSampleDurations().size) {
                    if (currentTime <= startTime) {

                        // current sample is still before the new starttime
                        startSample = currentSample
                    }
                    endSample = if (currentTime <= endTime) {
                        // current sample is after the new start time and still before the new endtime
                        currentSample
                    } else {
                        // current sample is after the end of the cropped video
                        break
                    }
                    currentTime += track.getSampleDurations().get(i).toDouble() / track.getTrackMetaData().getTimescale().toDouble()
                    currentSample++
                }
                movie.addTrack(CroppedTrack(track, startSample, endSample))
            }
            saveMovie(dst, movie)
            return endTime - startTime
        } finally {
            file.close()
        }

    }

    private fun saveMovie(dst: File, movie: Movie) {
        try{
            val out: Container = DefaultMp4Builder().build(movie)
          /*  val mvhd: MovieHeaderBox = Path.getPath(out, "moov/mvhd")
            mvhd.matrix = Matrix.ROTATE_180*/
            if (!dst.exists()) {
                dst.createNewFile()
            }
            val fos = FileOutputStream(dst)
            val fc: WritableByteChannel = fos.getChannel()
            try {
                out.writeContainer(fc)
            } finally {
                fc.close()
                fos.close()
            }
        }catch (e:Exception){
            Log.e("VideoParser",e.message)
        }
    }

    private fun correctTimeToSyncSample(track: Track, cutHere: Double, next: Boolean): Double {
        val timeOfSyncSamples = DoubleArray(track.getSyncSamples().size)
        var currentSample: Long = 0
        var currentTime = 0.0
        for (i in 0 until track.getSampleDurations().size) {
            val delta: Long = track.getSampleDurations().get(i)
            if (Arrays.binarySearch(track.getSyncSamples(), currentSample + 1) >= 0) {
                timeOfSyncSamples[Arrays.binarySearch(track.getSyncSamples(), currentSample + 1)] = currentTime
            }
            currentTime += delta.toDouble() / track.getTrackMetaData().getTimescale().toDouble()
            currentSample++
        }
        var previous = 0.0
        for (timeOfSyncSample in timeOfSyncSamples) {
            if (timeOfSyncSample > cutHere) {
                return if (next) {
                    timeOfSyncSample
                } else {
                    previous
                }
            }
            previous = timeOfSyncSample
        }
        return timeOfSyncSamples[timeOfSyncSamples.size - 1]
    }

    fun mergeVideos(files: MutableList<File>, saveTo: File) {
        if (files.size == 0) {
            return
        }
        if (files.size == 1) {
            files[0].copyTo(saveTo, true)
            return
        }
        val videoTracks = mutableListOf<Track>()
        val audioTracks = mutableListOf<Track>()
        var movie: Movie? = null
        val fileDatas = mutableListOf<FileDataSourceImpl>()
        val movies = mutableListOf<Movie>()
        //GeneralUtils.checkForPermissionsMAndAbove(context, true)
        var f : FileDataSourceImpl? = null;
        for ( file in files) {
            f = FileDataSourceImpl(file)
            movie = MovieCreator.build(f)
            movies.add(movie)
            fileDatas.add(f)
        }
        for (movie in movies) {
            for (track in movie.tracks) {
               // if (track.handler == "soun") audioTracks.add(track)
                if (track.handler == "vide") videoTracks.add(track)
                if (track.handler == "soun") audioTracks.add(track)
            }
        }
        val res = Movie()
        res.setTracks(LinkedList<Track>())
        res.addTrack(AppendTrack(*videoTracks.toTypedArray()))
        res.addTrack(AppendTrack(*audioTracks.toTypedArray()))
        saveMovie(saveTo, res)

        for (d in fileDatas) {
            d.close()
        }
    }

    suspend fun rotateOrFlipVideoAsync(editedFile: File, saveTo: File, context: Context, flip: Boolean
    ): Unit? = suspendCancellableCoroutine { cont ->
        val result = rotateOrFlipVideo(editedFile, saveTo, context, flip)
        cont.resume(result, onCancellation = {})
    }

    fun rotateOrFlipVideo(editedFile: File, saveTo: File, context: Context, flip: Boolean) {
//     //   if (rotation == Matrix.ROTATE_270) {
////            //   val commandComplete = arrayOf("ffmpeg", "-i", editedFile.absolutePath, "-vf", "transpose=1,transpose=1", saveTo.absolutePath)
////            var commandComplete = arrayOf("-i", editedFile.absolutePath, "-vf", "hflip", "-c:a", "copy", saveTo.absolutePath)
////            if (!flip) {
////                commandComplete = arrayOf("-i", editedFile.absolutePath, "-c", "copy", "-metadata:s:v:0", "rotate=90", saveTo.absolutePath)
////            }
////            //    $ ffmpeg -i input.mp4 -vf "transpose=2,transpose=2" output.mp4
////            //c copy -metadata:s:v:0 rotate=90
////            Log.d("VideoParser", "reducing " + saveTo.absolutePath)
////        when (val rc = FFmpeg.execute(commandComplete)) {
////            Config.RETURN_CODE_SUCCESS -> Log.i(Config.TAG, "Command execution completed successfully.")
////            Config.RETURN_CODE_CANCEL -> Log.i(Config.TAG, "Command execution cancelled by user.")
////            else -> {
////                Log.i(Config.TAG, "Command execution failed with $rc and the output below.")
////                Config.printLastCommandOutput(Log.INFO)
////            }
////        }

        //        val tmpFile =  MediaUtil.createFile(context, "mp4", "tmp_")

        try {
//            val command1 = "-i " + editedFile.absolutePath + " -vf scale=iw/3:ih/3 " + tmpFile.absolutePath
//            val rc1 = FFmpeg.execute(command1)
//            if (rc1 == Config.RETURN_CODE_SUCCESS) {
//                Log.i(Config.TAG, "Command execution completed successfully.")
//            } else if (rc1 == Config.RETURN_CODE_CANCEL) {
//                Log.i(Config.TAG, "Command execution cancelled by user.")
//            } else {
//                Log.i(Config.TAG, String.format("Command execution failed with rc=%d and the output below.", rc1))
//                Config.printLastCommandOutput(Log.INFO)
//            }
            val command2 = "-i " + editedFile.absolutePath + " -vf hflip -codec:v libx264 -codec:a copy " + saveTo.absolutePath
//            val command2 = "-i " + editedFile.absolutePath + " -vf hflip -c:a copy " + saveTo.absolutePath
            val rc = FFmpeg.execute(command2)
            if (rc == RETURN_CODE_SUCCESS) {
                Log.i(Config.TAG, "Command execution completed successfully.")
            } else if (rc == RETURN_CODE_CANCEL) {
                Log.i(Config.TAG, "Command execution cancelled by user.")
            } else {
                Log.i(Config.TAG, String.format("Command execution failed with rc=%d and the output below.", rc))
//                Config.printLastCommandOutput(Log.INFO)
            }
            Log.d("VideoParser", "ffmpeg4android finished successfully")

        } catch (e: Throwable) {
            Log.e("VideoParser", "vk run exception.", e)
        }
    }
}