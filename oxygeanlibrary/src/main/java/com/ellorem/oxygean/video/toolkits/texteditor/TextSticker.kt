package com.ellorem.oxygean.video.toolkits.texteditor

import android.text.SpannableString
import android.view.Gravity
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.FontDescriptor
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font.FontSize
import com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font.FontType
import java.io.File

data class TextSticker(val x: Float = 0f,
                       val y: Float = 0f,
                       val width: Int = 0,
                       val height: Int = 0,
                       val rotation: Float = 0f,
                       val textDescriptor: FontDescriptor? = null,
                       val editedText: SpannableString,
                       val gravity: Int = Gravity.CENTER,
                       val xP: Float = 0f,
                       val yP: Float = 0f,
                       val sizePX: Int = 500,
                       val isExist: Boolean = false,
                       val imageFile: File)