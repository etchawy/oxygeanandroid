package com.ellorem.oxygean.video.toolkits.texteditor.fonttoolkit.font

import android.graphics.Typeface

class Font(val onPictureRes: Int, val offPictureRes: Int, val font: Typeface){
    var isSwitched: Boolean = false
}