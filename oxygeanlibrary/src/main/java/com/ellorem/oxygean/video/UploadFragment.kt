package com.ellorem.oxygean.video

import android.content.ContentUris
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.ellorem.oxygean.video.util.MediaUtil
import com.ellorem.oxygean.video.util.VideoParser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream


class UploadFragment : Fragment(){

    val onChooseThumbnailListener = object : ChooseThumbnailAdapter.OnChooseThumbnailListener {
        override fun onHasChecks() {
            requireView().findViewById<View>(R.id.next_layout).visibility = View.VISIBLE
        }

        override fun onNoChecks() {
            requireView().findViewById<View>(R.id.next_layout).visibility = View.INVISIBLE
        }

    }

    var adapter : ChooseThumbnailAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_upload, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireView().findViewById<ImageView>(R.id.navigation).setOnClickListener({(requireActivity() as CameraActivity).removeCurrentFragment()})
        requireView().findViewById<View>(R.id.main).setVisibility(View.GONE)

        requireView().findViewById<View>(R.id.next_layout).setOnClickListener {
            val videosForUpload = adapter!!.forUpload
            val newVideos = mutableListOf<MediaUtil.VideoData>()
            requireView().findViewById<View>(R.id.loading_layout).setVisibility(View.VISIBLE)
            requireView().findViewById<View>(R.id.main).setVisibility(View.GONE)

            lifecycleScope.launch(Dispatchers.Default) {
                copyAndPrepareVideosAsync(videosForUpload, newVideos)
                requireActivity().runOnUiThread {
                    (requireActivity() as CameraActivity).startCameraScreen(newVideos)
                }
            }

        }

        lifecycleScope.launch(Dispatchers.Default) {
            val list = MediaUtil.getAllMediaAsync(requireContext())
            adapter = ChooseThumbnailAdapter(requireContext(), list)
            val gridView: GridView = (requireView().findViewById(R.id.gridview));
            gridView.adapter = adapter

            adapter!!.addListener(onChooseThumbnailListener)

            requireActivity().runOnUiThread {
                requireView().findViewById<View>(R.id.loading_layout).setVisibility(View.GONE)
                requireView().findViewById<View>(R.id.main).setVisibility(View.VISIBLE)
            }
        }
    }

    suspend fun copyAndPrepareVideosAsync(
            videosForUpload: MutableList<MediaUtil.VideoData>, newVideos: MutableList<MediaUtil.VideoData>
    ): Unit = suspendCancellableCoroutine { cont ->
        val result = copyAndPrepareVideos(videosForUpload, newVideos)
        cont.resume(result, onCancellation = {})
    }

    private fun copyAndPrepareVideos(videosForUpload: MutableList<MediaUtil.VideoData>, newVideos: MutableList<MediaUtil.VideoData>) {
        for (video in videosForUpload) {
            //videos more then 2 minutes

            video.shouldReduceSize = true
            var newFileOnApplicationLocalStorage = MediaUtil.createFile(requireContext(), "mp4")

            val resolver = requireContext().contentResolver
            resolver.openInputStream(ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, video.mediaId
            )).use { stream ->
                try {
                    val out: OutputStream = FileOutputStream(newFileOnApplicationLocalStorage)
                    try {
                        // Transfer bytes from in to out
                        val buf = ByteArray(1024)
                        var len: Int = 0
                        while (stream!!.read(buf).also({ len = it }) > 0) {
                            out.write(buf, 0, len)
                        }
                    } finally {
                        out.close()
                    }
                } finally {
                    stream!!.close()
                }
            }

            video.videoURL = newFileOnApplicationLocalStorage.absolutePath

            if (video.length > 120) {
                val cnt = video.length.toInt() / 120
                for (i in 0..cnt) {
                    val startTime = i * 120.0
                    var endTime = startTime + 120.0
                    if (i == cnt) {
                        endTime = video.length
                    }
                    val splitVideo = splitVideo(video, startTime, endTime)
                    splitVideo.shouldReduceSize = true
                    newVideos.add(splitVideo)
                }
            } else {
                newVideos.add(video)
            }
        }
    }


    fun splitVideo(currentVideo: MediaUtil.VideoData, startTime: Double, endTime: Double) : MediaUtil.VideoData {
        val editedFile =  MediaUtil.createFile(requireContext(), "mp4", "split_upload_")
        val durationFinal = VideoParser().startTrim(File(currentVideo!!.videoURL), editedFile, startTime, endTime, currentVideo!!.length, false)
        val thumbFile = MediaUtil.createFile(requireContext(), "jpeg")
        MediaUtil.createThumbFile(requireContext(), editedFile, thumbFile)
        return videoToAdd@MediaUtil.VideoData(
                durationFinal,
                listOf(),
                0,
                false,
                thumbFile.absolutePath,
                editedFile.length(),
                editedFile.absolutePath)
    }

    override fun onDestroyView() {
        if (adapter != null) {
            adapter!!.addListener(onChooseThumbnailListener)
        }
        super.onDestroyView()
    }

}